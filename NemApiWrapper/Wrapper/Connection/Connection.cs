﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;

namespace NemApi
{
    public class Connection
    {
        internal HttpClient Client = new HttpClient();
        internal UriBuilder Uri { get; set; }
        private List<string> PreTrustedNodes { get; set; }
        public bool ShouldFindNewHostIfRequestFails { get; set; }
        private byte NetworkVersion { get; set; }

        public Connection(UriBuilder uri,  byte networkVersion = 0x68)
        {
            this.Uri = uri;
            this.ShouldFindNewHostIfRequestFails = true;
            this.NetworkVersion = networkVersion;
            setLivenetPretrustedHostList();
        }

        public Connection()
        {
            this.Uri = new UriBuilder();
            this.Uri.Port = 7890;
            this.ShouldFindNewHostIfRequestFails = true;
            this.NetworkVersion = 0x68;
            this.setLivenetPretrustedHostList();
            this.setNewHost(); 
        }

        public void setTestNet()
        {
            this.NetworkVersion = 0x98;
            this.setTestnetPretrustedHostList();
            this.setNewHost();
        }

        public void setMainnet()
        {
            this.NetworkVersion = 0x68;
            this.setLivenetPretrustedHostList();
            this.setNewHost();
        }

        public void setNewHostList(List<string> hosts)
        {
            this.PreTrustedNodes = hosts;
        }

        public void setHost(string host)
        {
            this.Uri.Host = host;
        }

        public string getHost()
        {
            return this.Uri.Host;
        }

        public byte getNetworkVersion()
        {
            return this.NetworkVersion;
        }

        internal UriBuilder getUri()
        {
            return this.Uri;
        }

        internal UriBuilder getUri(string path)
        {
            this.Uri.Path = path;
            this.Uri.Query = null;
            return Uri;
        }

        internal UriBuilder getUri(string path, string query)
        {
            this.Uri.Path = path;
            this.Uri.Query = query;
            return Uri;
        }

        internal void setNewHost()
        {
            
            var rnd = new Random();
            var r = rnd.Next(this.PreTrustedNodes.Count);
            this.Uri.Host = this.PreTrustedNodes[r]; 
        }
        
        internal void setLivenetPretrustedHostList()
        {
            this.PreTrustedNodes = new List<string>()
            {
                "85.25.36.97",
                "85.25.36.92",
                "199.217.112.135",
                "108.61.182.27",
                "108.61.168.86",
                "104.238.161.61",
                "62.75.171.41",
                "san.nem.ninja",
                "go.nem.ninja",
                "hachi.nem.ninja"
            };
        }

        internal void setTestnetPretrustedHostList()
        {
            this.PreTrustedNodes = new List<string>()
            {
                 "192.3.61.243",
                 "23.228.67.85",
                 "bob.nem.ninja",
                 "50.3.87.123",
                 "192.3.61.243",
            };
        }   
    }
}
