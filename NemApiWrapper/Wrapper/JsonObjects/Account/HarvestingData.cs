﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class HarvestingData
    {
        public class BlockHash
        {
            public string data { get; set; }
        }

        public class Datum
        {
            public int timeStamp { get; set; }
            public BlockHash blockHash { get; set; }
            public int totalFee { get; set; }
            public int height { get; set; }
        }

        public class ListData
        {
            public List<Datum> data { get; set; }
        }
    }
}
