﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class PrivateKey
    {
        public SecureString RAW { get; private set; }
        
        public PrivateKey(SecureString key)
        {
            RAW = StringUtils.ConvertToUnsecureString(key).Length == 66 ? StringUtils.ToSecureString(StringUtils.ConvertToUnsecureString(key).Substring(2, key.Length - 2)) : key;
        }

        public PrivateKey(string key)
        {
            RAW = key.Length == 66 ? StringUtils.ToSecureString(key.Substring(2, key.Length - 2))
                                   : StringUtils.ToSecureString(key);
        }
    }
}
