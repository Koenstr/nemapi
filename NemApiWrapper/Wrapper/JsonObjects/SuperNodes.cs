﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class SuperNodes
    {
        public class Node
        {
            public string id { get; set; }
            public string alias { get; set; }
            public string ip { get; set; }
            public int nisPort { get; set; }
            public string pubKey { get; set; }
            public int servantPort { get; set; }
            public int status { get; set; }
            public double latitude { get; set; }
            public double longitude { get; set; }
            public string payoutAddress { get; set; }
        }

        public class RootObject
        {
            public List<Node> nodes { get; set; }
        }
    }
}
