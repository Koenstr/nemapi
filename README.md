WARNING: Still in development. Use at own risk. Fee structure not fully updated to new few structure.

Import NemApi.dll and reference.

	using NemApi;
	
Set up connection [OPTIONAL, AccountFactory defaults to main net trusted nodes]

	Connection con = new Connection();
	
	or to create a custom connection
	
	Connection con = new Connection(<UriBuilder>, <Network Byte>);

Set to main or test net

	con.setTestNet();
	
	con.setMainNet();

Set to find new host if currectly connected host is down [OPTIONAL, Default: true]

	con.ShouldFindNewHostIfRequestFails = true;

Set up account details

	var privateKey = new PrivateKey("");
	
	var pubKey = new PublicKey("");
	
	var encoded = new EncodedAddress("");

	var account1 = new AccountFactory(con).fromPrivateKey(privateKey);
	
	var account2 = new AccountFactory(con).fromPublicKey(pubKey);
	
	// Note: Public key, if available, is automatically retrieved 
	//       when encoded address is provided
	
	var account3 = new AccountFactory(con).fromEncodedAddress(encoded);

Unverifiable Account requests

    var newKeyPair = await unverifiableAccount.getGenerateNewAccountAsync(); // needs to be taken out of UnverifiableAccount class

	var existingAccountData = await unverifiableAccount.getAccountInfoAsync();
	
	var accountStatus = await unverifiableAccount.getAccountStatusAsync();

Account transactions

	var accountTransactions = await unverifiableAccount.getAllTransactionsAsync();
	
	var incomngTransactions = await unverifiableAccount.getIncomingTransactionsAsync();
	
	var outgoingTransactions = await unverifiableAccount.getOutgoingTransactionsAsync();
	
	var unconfirmedTransactions = await unverifiableAccount.getUnconfirmedTransactionsAsync();

Other

	var delegatedRoot = await unverifiableAccount.getDelegatedAccountRootAsync();
	
	var harvestingInfo = await unverifiableAccount.getHarvestingInfoAsync();
	
	var importances = await unverifiableAccount.getImportancesAsync();

Account namespace and mosaic requests

	var createdMosaics = await unverifiableAccount.getMosaicsAsync();
	
	var mosaicsByNameSpace = await unverifiableAccount.getMosaicsByNameSpaceAsync("<namespace>", "<Optional ID>", /*Optional page size*/ 1);
	
	var ownedMosaics = await unverifiableAccount.getMosaicsOwnedAsync();
	
	var provisionedNameSpaces = await unverifiableAccount.getNamespacesAsync();

Node to which request sent must have historic data retrieval enabled.
	
	var historicAccountData = await unverifiableAccount.HistoricData(unverifiableAccount.Encoded.ENCODED, 0,100,2);
	
Send transaction with mosaic and message

	var mosaicList = new List<Mosaic>
	{
		new Mosaic("", "", 0)
	};
	
	var transferData = new TransferTransactionData()
	{
		amount = 0,
		message = "",
		encrypted = true, // encoding still broken, encrypts wrapper to wrapper successfully but not compatible with ncc
		recipient = verifiableAccount.Encoded,
		listOfMosaics = mosaicList,
		multisigAccount = new PublicKey("")
	};
	
	var accountTransactionResponse = await verifiableAccount.sendTransactionAsync(transferData);
	
Importance transfer

	var importanceData = new ImportanceTransferData()
	{
		multisigAccount = null,
		activate = false,
		delegatedAccount = new PublicKey("")
	};

	var importanceTransferRespose = await verifiableAccount.importanceTransferAsync(importanceData);

Provision NameSpace
	
	var namespaceData = new ProvisionNameSpaceData()
	{
		newPart = "testmosaic1" 
	};
	
	var provisionNameSpaceResponse = verifiableAccount.provisionNamespaceAsync(namespaceData);
	
Mosaic Creation Transaction
	
	var data = new MosaicCreationData()
	{
		multisigAccount = unverifiableAccount.PublicKey,
		description = "",
		divisibility = 0,
		initialSupply = 0,
		supplyMutable = true,
		mosaicName = "",
		nameSpaceId = "",
		transferable = true
	};
	
	var response = verifiableAccount.createMosaicAsync(data).Result;

Mosaic supply change
	
	var changeData = new MosaicSupplyChangeData()
	{
		nameSpaceId = "",
		mosaicName = "",
		supplyChangeType = 0,
		delta = 0,
	};
	
	var mosaicSupplyChangeResponse = verifiableAccount.mosaicSupplychangeAsync(changeData);

Aggregate multisig modification transaction

	var modList = new List<AggregateModification>()
	{
		new AggregateModification(new PublicKey(""), 1),
		new AggregateModification(new PublicKey(""), 2),
	};

	var modData = new AggregateModificationData()
	{
		modifications = modList,
		multisigAccount = unverifiableAccount.PublicKey
	};

	var aggModTransactionResponse = verifiableAccount.aggregateMultisigModificationAsync(modData);

Boot Node

Boots node conected to in Connection provided to account1 when accountFactory created account1
using the public key of account1 as boot key

	verifiableAccount.bootNodeAsync("<node name>");
   