﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    internal static class ByteUtils
    {
        internal static byte[] TruncateByteArray(byte[] bytes, int len)
        {
            byte[] truncBytes = new byte[len];
            
            Array.Copy(bytes, 0, truncBytes, 0, len);
            
            return truncBytes;
        }

        internal static byte[] ConcatonatatBytes(byte[] a, byte[] b)
        {
            byte[] combined = new byte[a.Length + b.Length];

            Array.Copy(a, combined, a.Length);
            Array.Copy(b, 0, combined, a.Length, b.Length);

            return combined;
        }
    }
}
