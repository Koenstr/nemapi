﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    internal class AggregateModificatioList : Transaction
    {
        private Serializer Serializer { get; set; }
        private List<AggregateModification> ModList { get; set; }
        private byte[] ModificationBytes { get; set; }
        private byte[] Bytes { get; set; }
        private int RelativeChange { get; set; }
        internal int Length { get; set; }
        private long Fee { get; set; }

        internal AggregateModificatioList(Connection connection, PublicKey publicKey, AggregateModificationData data) 
            : base(connection, publicKey, data.deadline) // todo : fee
        {
            this.RelativeChange = data.relativeChange;
            this.Serializer = new Serializer();
            this.ModList = data.modifications;

            serialize();
            setFee();

            this.Bytes = ByteUtils.TruncateByteArray(Serializer.GetBytes(), this.Length);

            finalize();
        }
        private void setFee()
        {
            this.Fee += this.RelativeChange == 0 
                      ? (10 + (6 * this.ModList.Count)) * 1000000
                      : (10 + (6 * this.ModList.Count) + 6) * 1000000;
        }

        private void finalize()
        {
            this.UpdateFee(this.Fee);
            this.UpdateTransactionType(TransactionType.MultisigAggregateModification);
            this.UpdateTransactionVersion(TransactionVersion.VersionTwo);

            var tempBytes = new byte[getCommonTransactionBytes().Length + this.Length];

            Array.Copy(getCommonTransactionBytes(), tempBytes, getCommonTransactionBytes().Length);
            Array.Copy(this.Bytes, 0, tempBytes, getCommonTransactionBytes().Length, this.Length);

            this.Length = tempBytes.Length;
            this.ModificationBytes = tempBytes;
        }

        internal byte[] getBytes()
        {
            return this.ModificationBytes;
        }

        private void serialize()
        {
            this.Length += 4;
            this.Serializer.WriteInt(this.ModList.Count);

                foreach (AggregateModification mod in this.ModList)
                {
                    this.Length += StructureLength.AggregateModification + ByteLength.FourBytes;
                    this.Serializer.WriteInt(StructureLength.AggregateModification);
                    this.Serializer.WriteInt(mod.ModificationType);
                    this.Serializer.WriteInt(ByteLength.PublicKeyLength);
                    this.Serializer.WriteBytes(CryptoBytes.FromHexString(mod.PublicKey.RAW));
                }
            
            
            if(this.RelativeChange != 0){
                this.Length += StructureLength.RelativeChange + ByteLength.FourBytes;  
                this.Serializer.WriteInt(StructureLength.RelativeChange);
                this.Serializer.WriteInt(this.RelativeChange);
            }
            else{
                this.Length += ByteLength.FourBytes;
                this.Serializer.WriteInt(ByteLength.ZERO);
            }
        }
    }  
}
