﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    /*
    * A list of mosaics which can be serilized.
    */
    internal class MosaicList
    {
        private PublicKey Sender { get; set; }
        private Connection Connection { get; set; }
        private List<Mosaic> ListOfMosaics { get; set; }
        private Serializer Serializer { get; set; }
        internal int Length { get; set; }
        private long Fee { get; set; }
        private byte[] Bytes { get; set; }

        internal MosaicList(List<Mosaic> mosaicList, Connection connection, PublicKey sender)
        {
            this.Sender = sender;
            this.Connection = connection;
            this.Serializer = new Serializer();
            this.ListOfMosaics = mosaicList;

            serialize();

            Bytes = ByteUtils.TruncateByteArray(Serializer.GetBytes(), Length);
            calculateMosaicTransferFee();
        }

        internal byte[] getMosaicListBytes()
        {
            return Bytes;
        }

        private void calculateMosaicTransferFee()
        {
            foreach (Mosaic mosaic in this.ListOfMosaics)
            {
                long q = 0;
                int  d = 0;
                long s = 0;

                var account = new UnverifiableAccount(Connection, Sender);
                var mosaicDefinitions = account.getMosaicsAsync().Result;

                foreach (MosaicDefinitions.Mosaic data in mosaicDefinitions.data)
                {
                    if (data.id.name == mosaic.MosaicName)
                    {
                        q = mosaic.Quantity;
                        d = Convert.ToInt32(data.properties[0].value);
                        s = Convert.ToInt64(data.properties[1].value);
                        break;
                    }
                    
                }
                if (q != 0)
                {
                    
                    if (s < 10000000000 && d == 0)
                    {
                        this.Fee = 1000000;
                    }
                    else
                    {
                        var xemEquivilent = ((8999999999 * q) / (s * 10 ^ d) * 1000000); // same as before.

                        this.Fee += (xemEquivilent / 1000000) < 8
                         ? (10 - (xemEquivilent / 1000000)) * 1000000
                         : (long)Math.Max(2, 99 * Math.Atan((xemEquivilent / 1000000) / 150000.0)) * 1000000;


                        this.Fee = (this.Fee / 100) * 125;
                    }
                   
                }
                else throw new Exception("mosaic not found");

                
            }
            
        }
        internal long getFee()
        {
            return this.Fee;
        }

        /*
        * Serializes the list of mosaics one by one.
        *
        * If there are no mosaics, an empty 4 bytes are serialized in place of the mosaics.
        *
        */
        private void serialize()
        {        
            Serializer.WriteInt(this.ListOfMosaics.Count);


            foreach (Mosaic mosaic in this.ListOfMosaics)
            {
                Length += mosaic.LengthOfMosaicStructure + ByteLength.FourBytes;

                Serializer.WriteInt(mosaic.LengthOfMosaicStructure);

                Serializer.WriteInt(mosaic.LengthOfMosaicIdStructure);

                Serializer.WriteInt(mosaic.LengthOfNameSpaceId);

                Serializer.WriteString(mosaic.NameSpaceId);

                Serializer.WriteInt(mosaic.LengthOfMosaicName);

                Serializer.WriteString(mosaic.MosaicName);

                Serializer.WriteLong(mosaic.Quantity);
            }    
        }
    }
}
