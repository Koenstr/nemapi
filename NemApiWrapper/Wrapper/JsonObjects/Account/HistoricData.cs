﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class HistoricData
    {
        public int height { get; set; }
        public string address { get; set; }
        public long balance { get; set; }
        public long vestedBalance { get; set; }
        public long unvestedBalance { get; set; }
        public double importance { get; set; }
        public double pageRank { get; set; }
    }
}
