﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    internal class MultiSigTransaction
    { 
        private Serializer Serializer { get; set; }
        private PublicKey PublicKey { get; set; }
        private int Deadline { get; set; }
        private byte[] MultiSigBytes { get; set; }
        private byte NetworkVersion { get; set; }
        private int TimeStamp { get; set; }
        private long Fee { get; set; }
        private int InnerLength { get; set; }

        internal MultiSigTransaction(Connection connection, PublicKey publicKey, PrivateKey privateKey, int deadline, int length) 
           
        {
            if (null == connection)
                throw new ArgumentNullException("Connection cannot be null");
            if (null == publicKey)
                throw new ArgumentNullException("Public key cannot be null");

            this.InnerLength = length;

            this.Serializer = new Serializer();

            this.NetworkVersion = connection.getNetworkVersion();

            this.TimeStamp = TimeDateUtils.EpochTimeInMilliSeconds();

            this.Deadline = deadline == 0 ? this.TimeStamp + 1000 : this.TimeStamp + deadline;

            this.PublicKey = publicKey;

            this.Fee = TransactionFee.MultisigWrapper;

            this.Serialize();

            this.MultiSigBytes = ByteUtils.TruncateByteArray(Serializer.GetBytes(), StructureLength.MultiSigHeader);

            this.MultiSigBytes[7] = NetworkVersion;
        }

        internal byte[] getBytes()
        {
            return MultiSigBytes;
        }

        private void Serialize()
        {
            // transaction type. Set as null/empty bytes as it will be 
            // updated when serializing the different transaction types.
            this.Serializer.WriteInt(TransactionType.MultisigTransaction);

            // version
            this.Serializer.WriteInt(TransactionVersion.VersionOne);

            // timestamp
            this.Serializer.WriteInt(this.TimeStamp);

            //pubKey len
            this.Serializer.WriteInt(ByteLength.PublicKeyLength);

            // pub key
            IEnumerable<string> set = Split(PublicKey.RAW, 2);
            byte[] bytes = new byte[32];
            int i = 0;
            foreach (string s in set)
            {
                int x = int.Parse(s, System.Globalization.NumberStyles.HexNumber);
                bytes[i] = (byte)x;
                i++;

            }
            this.Serializer.WriteBytes(bytes);

            // fee
            this.Serializer.WriteLong(this.Fee);

            // deadline
            this.Serializer.WriteInt(this.Deadline);

            this.Serializer.WriteInt(InnerLength);
        }

        static IEnumerable<string> Split(string str, int chunkSize)
        {
            return Enumerable.Range(0, str.Length / chunkSize)
                .Select(i => str.Substring(i * chunkSize, chunkSize));
        }
    }
}

