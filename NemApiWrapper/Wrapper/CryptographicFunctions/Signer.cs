﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{    
    internal class Signature
    {
        internal byte[] LongSignature { get; private set; }

        internal Signature(byte[] data, PrivateKey PrivateKey)
        {
            var sig = new byte[64];

            try
            {            
                var sk = new byte[64];
                Array.Copy(CryptoBytes.FromHexString(StringUtils.ConvertToUnsecureString(PrivateKey.RAW)), sk, 32);
                Array.Copy(GetKeyBytes(new PublicKey(CryptoBytes.ToHexStringLower(PublicKeyConversion.ToPublicKey( PrivateKey))).RAW), 0, sk, 32, 32);
                Ed25519.crypto_sign2(sig, data, sk, 32);
                CryptoBytes.Wipe(sk);   
            }
            finally
            {
                LongSignature = sig;               
            }
        }

        internal byte[] GetKeyBytes(string RAW)
        {
            IEnumerable<string> set = Split(RAW, 2);

            byte[] bytes = new byte[32];
            int i = 0;
            foreach (string s in set)
            {
                int x = int.Parse(s, System.Globalization.NumberStyles.HexNumber);
                bytes[i] = (byte)x;
                i++;

            }

            return bytes;
        }

        private static IEnumerable<string> Split(string str, int chunkSize)
        {
            return Enumerable.Range(0, str.Length / chunkSize)
                .Select(i => str.Substring(i * chunkSize, chunkSize));
        }
    }
}
