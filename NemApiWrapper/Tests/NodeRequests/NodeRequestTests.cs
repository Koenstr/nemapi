﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NemApi;

namespace Tests.NodeRequests
{
    [TestClass]
    public class NodeRequestTests
    {
        [TestMethod]
        public void CanRetrieveActivePeerList()
        {
            Connection con = new Connection();

            var nodeRequests = new Node(con);

            var response = nodeRequests.ActivePeerList();

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveExtendedNodeInfo()
        {
            Connection con = new Connection();

            var nodeRequests = new Node(con);

            var response = nodeRequests.ExtendedNodeInfo();

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveNodeInfo()
        {
            Connection con = new Connection();

            var nodeRequests = new Node(con);

            var response = nodeRequests.Info();

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveMaxHeight()
        {
            Connection con = new Connection();

            var nodeRequests = new Node(con);

            var response = nodeRequests.MaxHeight();

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrievePeerList()
        {
            Connection con = new Connection();

            var nodeRequests = new Node(con);

            var response = nodeRequests.PeerList();

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveReachablePeerList()
        {
            Connection con = new Connection();

            var nodeRequests = new Node(con);

            var response = nodeRequests.ReachablePeerList();

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveSuperNodeList()
        {
            Connection con = new Connection();

            var nodeRequests = new Node(con);

            var response = nodeRequests.SuperNodeList();

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveUnlockedInfo()
        {
            Connection con = new Connection();

            var nodeRequests = new Node(con);

            var response = nodeRequests.UnlockedInfo();

            Assert.IsNotNull(response);
        }
    }
}
