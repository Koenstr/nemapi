﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class BootNodeMetaData
    {
        public string application { get; set; }
    }

    public class BootNodeEndpoint
    {
        public string protocol { get; set; }
        public int port { get; set; }
        public string host { get; set; }
    }

    public class BootNodeIdentity
    {
        public string privateKey { get; set; }
        public string name { get; set; }
    }

    public class BootNodeRootObject
    {
        public BootNodeMetaData metaData { get; set; }
        public BootNodeEndpoint endpoint { get; set; }
        public BootNodeIdentity identity { get; set; }
    }
    public class UnlockedInfo
    {
        [JsonProperty("num-unlocked")]
        public int numUnlocked { get; set; }

        [JsonProperty("max-unlocked")]
        public int maxUnlocked { get; set; }
    }

    public class Height
    {
        public int height { get; set; }
    }

    public class MetaData
    {
        public string application { get; set; }
        public string version { get; set; }
        public string platform { get; set; }
    }

    public class Endpoint
    {
        public string protocol { get; set; }
        public int port { get; set; }
        public string host { get; set; }
    }

    public class Identity
    {
        public string name { get; set; }
        [JsonProperty("private-key")]
        public string privateKey { get; set; }
    }
    public class NodeList
    {
        public List<NodeData> data { get; set; }
    }

    public class NodeData
    {
        public MetaData metaData { get; set; }
        public Endpoint endpoint { get; set; }
        public Identity identity { get; set; }
    }

    public class NisInfo
    {
        public int currentTime { get; set; }
        public string application { get; set; }
        public int startTime { get; set; }
        public string version { get; set; }
        public string signer { get; set; }
    }

    public class NodeAndNis
    {
        public NodeData node { get; set; }
        public NisInfo nisInfo { get; set; }
    }
}
