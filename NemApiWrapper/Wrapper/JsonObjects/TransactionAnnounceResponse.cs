﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    /*
    * Stores the transaction announce response
    */
    public class NemAnnounceResponse
    {
        public class TransactionHash
        {
            public string data { get; set; }
        }

        public class InnerTransactionHash
        {
            public string data { get; set; }
        }

        public class Response
        {
            public int type { get; set; }
            public int code { get; set; }
            public string message { get; set; }
            public TransactionHash transactionHash { get; set; }
            public InnerTransactionHash innerTransactionHash { get; set; }
        }
    }

}
