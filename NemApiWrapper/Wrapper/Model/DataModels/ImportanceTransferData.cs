﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class ImportanceTransferData
    {
        public PublicKey multisigAccount { get; set; }
        public PublicKey delegatedAccount { get; set; }
        public bool activate { get; set; }
        public int deadline { get; set; }
    }
}
