﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class Node
    {
        private Connection Connection { get; set; }

        public Node(Connection connection)
        {
            if (null == connection)
                throw new ArgumentNullException("Connection cannot be null");

            Connection = connection;
        }

        public async Task<NodeData> Info()
        {
            return await new AsyncConnector.GetAsync<NodeData>(Connection).get("/node/info");
        }

        public async Task<SuperNodes.RootObject> SuperNodeList()
        {
            
            UriBuilder UriTemp = new UriBuilder("http://199.217.113.179/nodes");

            return await new AsyncConnector.GetAsync<SuperNodes.RootObject>(new Connection(UriTemp)).get("/nodes");
        }

        public async Task<NodeList> PeerList()
        {
            return await new AsyncConnector.GetAsync<NodeList>(Connection).get("/node/peer-list/all");
        }

        public async Task<NodeAndNis> ExtendedNodeInfo()
        {
            return await new AsyncConnector.GetAsync<NodeAndNis>(Connection).get("/node/extended-info");
        }

        public async Task<NodeList> ActivePeerList()
        {
            return await new AsyncConnector.GetAsync<NodeList>(Connection).get("/node/peer-list/active");
        }

        public async Task<UnlockedInfo> UnlockedInfo()
        {
            return await new AsyncConnector.GetAsync<UnlockedInfo>(Connection).get("account/unlocked/info");
        }

        public async Task<NodeList> ReachablePeerList()
        {
            return await new AsyncConnector.GetAsync<NodeList>(Connection).get("/node/peer-list/reachable");
        }

        public async Task<Height> MaxHeight()
        {
            return await new AsyncConnector.GetAsync<Height>(Connection).get("/node/active-peers/max-chain-height");
        }
    }
}
