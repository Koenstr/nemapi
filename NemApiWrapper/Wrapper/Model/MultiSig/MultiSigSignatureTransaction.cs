﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    internal class MultisigSignature : Transaction
    {
        private byte[] TransferBytes { get; set; }
        private byte[] SignatureBytes { get; set; }
        private MultisigSignatureTransactionData Data {get; set;}
        internal int FullLength { get; set; }
        private Serializer Serializer { get; set; }

        internal MultisigSignature(Connection connection, PublicKey senderPublicKey, MultisigSignatureTransactionData data)
            : base(connection, senderPublicKey, data.deadline)
        {
            this.Serializer = new Serializer();
            this.Data = data;
            this.serialize();
            this.TransferBytes = ByteUtils.TruncateByteArray(Serializer.GetBytes(), StructureLength.MultisigSignature);
            finalize();
        }

        internal void finalize()
        {
            UpdateFee(TransactionFee.MultisigSignature);
            UpdateTransactionType(TransactionType.MultisigSignatureTransaction);
            UpdateTransactionVersion(TransactionVersion.VersionOne);

            byte[] bytes = new byte[getCommonTransactionBytes().Length + StructureLength.MultisigSignature];

            Array.Copy(getCommonTransactionBytes(), bytes, getCommonTransactionBytes().Length);
            Array.Copy(TransferBytes, 0, bytes, getCommonTransactionBytes().Length, StructureLength.MultisigSignature);

            this.FullLength = bytes.Length;
            this.SignatureBytes = bytes;
        }

        internal byte[] getBytes()
        {
            return this.SignatureBytes;
        }

        private void serialize()
        {
            this.Serializer.WriteInt(0x24);

            this.Serializer.WriteInt(0x20);

            this.Serializer.WriteBytes(CryptoBytes.FromHexString(Data.transactionHash));

            this.Serializer.WriteInt(0x28);

            this.Serializer.WriteBytes(Encoding.UTF8.GetBytes(Data.MultisigAddress.ENCODED));       
        }
    }
}
