﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class GeneratedKeyPair
    {
        public string privateKey { get; set; }
        public string address { get; set; }
        public string publicKey { get; set; }
    }
}
