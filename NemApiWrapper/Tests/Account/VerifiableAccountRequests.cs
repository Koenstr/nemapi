﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NemApi;
using System.Diagnostics;

/*
/ Note: when running these tests, run on testnet and 
/ credit the accounts in TestConstants with some XEM.
/ 
/ The second time they are run, you may need to uncomment
/ the commented tests and comment out the test above them
/ and vice versa for the third test. its the easiest way to
/ fully test that all transaction types dinifitely work.
*/

namespace Tests.Account
{
    [TestClass]
    public class VerifiableAccountRequestComponents
    {
        [TestMethod]
        public void CanSendTransaction()
        {
            Connection con = new Connection();
            con.setTestNet();

            PrivateKey key = new PrivateKey(TestConstants.privKey);

            VerifiableAccount account = new AccountFactory(con).fromPrivateKey(key);
            EncodedAddress recip = new EncodedAddress(TestConstants.address);

            var data = new TransferTransactionData()
            {
                recipient = recip,
                amount = 55000000000
            };

            var response = account.sendTransactionAsync(data).Result;
            Console.Write(response.code);
            Assert.AreEqual(response.code, 1);
        }
        
        [TestMethod]
        public void CanSendMultiSigTransaction()
        {
            Connection con = new Connection();
            PrivateKey key = new PrivateKey(TestConstants.privKey);
            
            VerifiableAccount account = new AccountFactory(con).fromPrivateKey(key);
            UnverifiableAccount account2 = new AccountFactory(con).fromEncodedAddress(TestConstants.address);
            EncodedAddress recip = new EncodedAddress(TestConstants.address);

            var data = new TransferTransactionData()
            {
                multisigAccount = account2.PublicKey,
                recipient = recip,
                amount = 100
            };

            var response = account.sendTransactionAsync(data).Result;

            Assert.AreEqual(response.code, 8);
        }
        
        [TestMethod]
        public void CanModifiyMultisigAccount_add()
        {
            Connection con = new Connection();
            PrivateKey key = new PrivateKey(TestConstants.privKey);

            VerifiableAccount account = new AccountFactory(con).fromPrivateKey(key);
            
            AggregateModificationData data = new AggregateModificationData()
            {
                modifications = new List<AggregateModification>()
                {
                    new AggregateModification(TestConstants.listOfPublicKeys[0], 2),
                },    
                relativeChange = -1
            };
            
            var response = account.aggregateMultisigModificationAsync(data).Result;
            Console.Write(response.message);
            Assert.AreEqual(response.code, 1);
        }

        
        [TestMethod]
        public void CanModifiyMultisigAccount_remove()
        {
            Connection con = new Connection();
            PrivateKey key = new PrivateKey("c83ce30fcb5b81a51ba58ff827ccbc0142d61c13e2ed39e78e876605da16d8d7"); // one of the cosignatories

            VerifiableAccount account = new AccountFactory(con).fromPrivateKey(key);
            EncodedAddress recipient = new EncodedAddress(TestConstants.address);

            AggregateModificationData data = new AggregateModificationData()
            {
                modifications = new List<AggregateModification>()
                {
                    new AggregateModification(TestConstants.listOfPublicKeys[0], 2),
                    new AggregateModification(TestConstants.listOfPublicKeys[1], 2),
                    new AggregateModification(TestConstants.listOfPublicKeys[2], 2),
                }
            };


            var response = account.aggregateMultisigModificationAsync(data).Result;

            Assert.AreEqual(response.code, 1);
        }
        

        [TestMethod]
        public void CanProvisionNameSpace()
        {
            Connection con = new Connection();
            PrivateKey key = new PrivateKey(TestConstants.privKey);

            VerifiableAccount account = new AccountFactory(con).fromPrivateKey(key);
            EncodedAddress recipient = new EncodedAddress(TestConstants.address);

            ProvisionNameSpaceData data = new ProvisionNameSpaceData()
            {
                parent = "parent",
                newPart = "newpart"
            };

            var response = account.provisionNamespaceAsync(data).Result;

            Assert.AreEqual(response.code, 1);
        }

        [TestMethod]
        public void CanTransferImportance()
        {
            Connection con = new Connection();
            PrivateKey key = new PrivateKey(TestConstants.privKey);

            VerifiableAccount account = new AccountFactory(con).fromPrivateKey(key);

            ImportanceTransferData data = new ImportanceTransferData()
            {
                activate = true,
                delegatedAccount = TestConstants.listOfPublicKeys[2],

            };

            var response = account.importanceTransferAsync(data).Result;
            Console.Write(response.code);
            Assert.AreEqual(response.code, 1);
        }
        
        [TestMethod]
        public void CanCancleImportanceTransfer()
        {
            Connection con = new Connection();
            PrivateKey key = new PrivateKey(TestConstants.privKey);

            VerifiableAccount account = new AccountFactory(con).fromPrivateKey(key);
           
            ImportanceTransferData data = new ImportanceTransferData()
            {
                activate = false,
                delegatedAccount = TestConstants.listOfPublicKeys[2]
            };

            var response = account.importanceTransferAsync(data).Result;

            Assert.AreEqual(response.code,1);
        }
        
    }
}
