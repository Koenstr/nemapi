﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    internal class ImportanceTransfer : Transaction
    {
        private PublicKey PublicKey { get; set; }
        private byte[] TransferMode { get; set; }
        private byte[] TransferBytes { get; set; }
        private byte[] ImportanceBytes { get; set; }
        internal int FullLength { get; set; }
        private Serializer Serializer { get; set; }

        internal ImportanceTransfer(Connection connection, PublicKey senderPublicKey, ImportanceTransferData data)
            : base(connection, senderPublicKey, data.deadline)
        {
            if (!StringUtils.OnlyHexInString(data.delegatedAccount.RAW) || data.delegatedAccount.RAW.Length != 64)
                throw new ArgumentNullException("Invalid public key");

            this.Serializer = new Serializer();
            this.PublicKey = data.delegatedAccount;
            this.TransferMode = data.activate == true ? DefaultBytes.Activate : DefaultBytes.Deactivate;

            this.serialize();
            this.TransferBytes = ByteUtils.TruncateByteArray(Serializer.GetBytes(), StructureLength.ImportnaceTransfer);
            finalize();
        }

        internal void finalize()
        {
            UpdateFee(TransactionFee.ImportanceTransfer);
            UpdateTransactionType(TransactionType.ImportanceTransfer);
            UpdateTransactionVersion(TransactionVersion.VersionOne);

            byte[] bytes = new byte[getCommonTransactionBytes().Length + StructureLength.ImportnaceTransfer];

            Array.Copy(getCommonTransactionBytes(), bytes, getCommonTransactionBytes().Length);
            Array.Copy(TransferBytes, 0, bytes, getCommonTransactionBytes().Length, StructureLength.ImportnaceTransfer);

            this.FullLength = bytes.Length;
            this.ImportanceBytes = bytes;
        }

        internal byte[] getBytes()
        {
            return this.ImportanceBytes;
        }

        private void serialize()
        {
            this.Serializer.WriteBytes(this.TransferMode);

            this.Serializer.WriteInt(ByteLength.PublicKeyLength);

            this.Serializer.WriteBytes(CryptoBytes.FromHexString(this.PublicKey.RAW));
        }
    }  
}
