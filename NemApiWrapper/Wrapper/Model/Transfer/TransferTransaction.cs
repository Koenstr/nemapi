﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    /*
    * Serializable transfer transaction data
    */
    internal class TransferTransaction : Transaction
    {
        private Serializer Serializer = new Serializer();
        private PrivateKey SenderPrivateKey { get; set; }
        private PublicKey SenderPublicKey { get; set; }
        private Connection Connection { get; set; }
        private TransferTransactionData Data { get; set; }

        internal int FullLength { get; set; }
        internal int MessageLength { get; set; }
        internal int MosaicLength { get; set; }
        internal long Fee { get; set; }

        private byte[] TransferMessageMosaicBytes { get; set; }
        private byte[] TransferBytes { get; set; }
        private byte[] MosaicBytes { get; set; }

        internal TransferTransaction(Connection connection, PublicKey senderPublicKey, PrivateKey senderPrivateKey, TransferTransactionData transactionData)
            : base(connection, senderPublicKey, transactionData.deadline) // todo : fee
        {
            this.SenderPublicKey = senderPublicKey;
            this.MessageLength = StructureLength.TransferTransaction;
            this.Data = transactionData;
            this.Connection = connection;
            this.SenderPrivateKey = senderPrivateKey;

            
            this.SerializeTrasnferPart();
            this.SerializeAttachments();

            this.TransferBytes = ByteUtils.TruncateByteArray(Serializer.GetBytes(), MessageLength + MosaicLength);

            finalize();   
        }

        private void SerializeAttachments()
        {
            if (this.Data.message != null)
            {
                this.SerializeMessagePart();
            }
            else this.Serializer.WriteInt(ByteLength.ZERO);

            if (this.Data.listOfMosaics != null)
            {
                this.SerializeMosaicPart();
            }
            else this.Serializer.WriteInt(ByteLength.ZERO);
        }

        internal byte[] getTransferBytes()
        {
            return TransferMessageMosaicBytes;
        }

        private void finalize()
        {
            this.UpdateFee(this.Fee );
            this.UpdateTransactionType(TransactionType.TransferTransaction);
            this.UpdateTransactionVersion(TransactionVersion.VersionTwo);

            byte[] bytes = new byte[getCommonTransactionBytes().Length + this.MessageLength + this.MosaicLength];

            Array.Copy(getCommonTransactionBytes(), bytes, getCommonTransactionBytes().Length);
            Array.Copy(this.TransferBytes, 0, bytes, getCommonTransactionBytes().Length, this.MessageLength + this.MosaicLength);

            this.FullLength = bytes.Length;
            this.TransferMessageMosaicBytes = bytes;
        }

        private void SerializeTrasnferPart()
        {
            this.Serializer.WriteInt(ByteLength.AddressLength);
            this.Serializer.WriteString(this.Data.recipient.ENCODED);
            this.Serializer.WriteLong(this.Data.amount);
            this.Fee += Math.Max(1, Math.Min((long)Math.Ceiling(((decimal)Data.amount / 10000000000)  ), 25)) * 1000000;
        }

        private void SerializeMessagePart()
        {
            Message serializeMessage = new Message(this.Connection, this.SenderPrivateKey, this.Data.recipient.ENCODED, this.Data.message, this.Data.encrypted);
            this.Serializer.WriteBytes(serializeMessage.getMessageBytes());
            this.MessageLength += serializeMessage.Length;
            this.Fee += serializeMessage.getFee();
        }

        private void SerializeMosaicPart()
        {
            MosaicList mosaicList = new MosaicList(this.Data.listOfMosaics, this.Connection, this.SenderPublicKey);
            this.Serializer.WriteBytes(mosaicList.getMosaicListBytes());
            this.MosaicLength = mosaicList.getMosaicListBytes().Length;
            this.Fee += mosaicList.getFee();
        }
    }
}