﻿using Chaos.NaCl;

using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace NemApi
{
    internal class AsyncConnector
    {
        internal class PostAsync
        {
            internal dynamic RPA { get; set; }
            private Connection Connection { get; set; }
            
            internal PostAsync(Connection connection)
            {
                if (null == connection)
                {
                    throw new ArgumentNullException("Connection cannot be null");
                }

                this.Connection = connection;
      
            }

            internal async Task post<TPostType>(string path, TPostType value)
            {
                await this.Connection.Client.PostAsync(
                    this.Connection.getUri(path).Uri,
                    new StringContent(JsonConvert.SerializeObject(value).ToString(),
                        Encoding.UTF8,
                        "application/json"));
            }

            internal async Task<NemAnnounceResponse.Response> send()
            {
                try
                {
                    var response = await this.Connection.Client.PostAsync(
                            this.Connection.getUri("transaction/announce").Uri,
                            new StringContent(JsonConvert.SerializeObject(this.RPA).ToString(),
                                Encoding.UTF8, 
                                "application/json"));

                    return JsonConvert.DeserializeObject<NemAnnounceResponse.Response>(
                        await response.Content.ReadAsStringAsync());
                }
                catch (HttpRequestException)
                {
                    if (this.Connection.ShouldFindNewHostIfRequestFails)
                    {
                        this.Connection.setNewHost();
                        return await send();
                    }
                    else throw new HttpRequestException();
                }            
            }
        }

        internal class GetAsync<TReturnType>
        {
            private Connection Connection { get; set; }

            internal GetAsync(Connection connection)
            {
                this.Connection = connection;
             
            }

            internal async Task<TReturnType> get(string path, string query = "")
            {
                try
                {
                    
                    var response = await this.Connection.Client.GetAsync(Connection.getUri(path, query).Uri);
                   
                    return JsonConvert.DeserializeObject<TReturnType>(await response.Content.ReadAsStringAsync());
                }
                catch (HttpRequestException e)
                {

                    if (this.Connection.ShouldFindNewHostIfRequestFails)
                    {                      
                        this.Connection.setNewHost();
                        return await get(path, query);             
                    }
                    else throw e;
                }
            }

            internal async Task<TReturnType> get<TPostType>(string path, TPostType value)
            {
                try
                {
                   
                    var response = await Connection.Client.PostAsync(
                         this.Connection.getUri(path).Uri,
                         new StringContent(JsonConvert.SerializeObject(value).ToString(), Encoding.UTF8, "application/json"));
                
                    return JsonConvert.DeserializeObject<TReturnType>(await response.Content.ReadAsStringAsync());
                }
                catch (HttpRequestException e)
                {
                    if (this.Connection.ShouldFindNewHostIfRequestFails)
                    {
                        this.Connection.setNewHost();
                        return await get(path, value);
                    }
                    else throw e;
                }
            }
        }
    }
}
