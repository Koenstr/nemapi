﻿using Chaos.NaCl;
using Org.BouncyCastle.Crypto.Digests;
using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace NemApi
{
    public class AddressEncoding
    {
        /*
        * Converts a provided public key to an encoded address
        *
        * @Param: publicKey, The key to convert to an encoded address
        *
        * @Returns: EncodedAddress
        */
        public static string ToEncoded(byte network, PublicKey publicKey)
        {
            if (!StringUtils.OnlyHexInString(publicKey.RAW) || (publicKey.RAW.Length != 64 && publicKey.RAW.Length != 66))
                throw new ArgumentException("invalid public key");

            // step 1) sha-3(256) public key
            var DigestSha3 = new KeccakDigest(256);
            byte[] stepOne = new byte[32];
            
            DigestSha3.BlockUpdate(CryptoBytes.FromHexString(publicKey.RAW), 0, 32);
            DigestSha3.DoFinal(stepOne, 0);

            // step 2) perform ripemd160 on previous step
            var DigestRipeMD160 = new RipeMD160Digest();
            byte[] stepTwo = new byte[20];
            DigestRipeMD160.BlockUpdate(stepOne, 0, 32);
            DigestRipeMD160.DoFinal(stepTwo, 0);

            // step3) prepend network byte    
            var stepThree = CryptoBytes.FromHexString(string.Concat(network == 0x68 ? 68 : 98, CryptoBytes.ToHexStringLower(stepTwo)));

            // step 4) perform sha3 on previous step
            var stepFour = new byte[32];
            DigestSha3.BlockUpdate(stepThree, 0, 21);
            DigestSha3.DoFinal(stepFour, 0);

            // step 5) retrieve checksum
            byte[] stepFive = new byte[4];
            Array.Copy(stepFour, 0, stepFive, 0, 4);

            // step 6) append stepFive to resulst of stepThree
            byte[] stepSix = new byte[25];
            Array.Copy(stepThree, 0, stepSix, 0, 21);
            Array.Copy(stepFive, 0, stepSix, 21, 4);

            // step 7) return base 32 encode address byte array
            return new Base32Encoder().Encode(stepSix).ToUpper();
        }
    }
}
