﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public static class StringUtils
    {
        public static string ConvertToUnsecureString(SecureString securePassword)
        {
            if (securePassword == null)
                throw new ArgumentNullException("securePassword");

            IntPtr unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                return Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }

        public static SecureString ToSecureString(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
                return null;
            else
            {
                SecureString result = new SecureString();
                foreach (char c in source.ToCharArray())
                    result.AppendChar(c);
                return result;
            }
        }

        internal static bool OnlyHexInString(SecureString data) 
        {
            if (null == data)
            {
                throw new ArgumentNullException("argument cannot be null");
            }
            return System.Text.RegularExpressions.Regex.IsMatch(StringUtils.ConvertToUnsecureString(data), @"\A\b[0-9a-fA-F]+\b\Z");
        }
        internal static bool OnlyHexInString(string data)
        {
            if (null == data)
            {
                throw new ArgumentNullException("argument cannot be null");
            }
            return System.Text.RegularExpressions.Regex.IsMatch(data, @"\A\b[0-9a-fA-F]+\b\Z");
        }
        public static string GetResultsWithHyphen(string input)
        {
            string output = "";
            int start = 0;

            while (start < input.Length)
            {
                output += input.Substring(start, Math.Min(6, input.Length - start)) + "-";
                start += 6;
            }

            return output.Trim('-');
        }
        public static string GetResultsWithoutHyphen(string input)
        {
            return input.Replace("-", "");
        }
    }
    
}
