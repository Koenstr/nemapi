﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace NemApi.Model.Account
{
    public class KeyPair
    {
        public PrivateKey privateKey { get; set; }
        public PublicKey publicKey { get; set; }
        public string Address { get; set; }
        public KeyPair(string sk)
        {
            this.privateKey = new PrivateKey(sk);
            this.publicKey = new PublicKey(privateKey);
        }
        public KeyPair(SecureString sk)
        {
            this.privateKey = new PrivateKey(sk);
            this.publicKey = new PublicKey(privateKey);
        }
        public KeyPair()
        {
            this.privateKey = new PrivateKey("");
            this.publicKey = new PublicKey(this.privateKey);
        }
    }
}
