﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class EncodedAddress
    {
        public string ENCODED { get; private set; }

        public EncodedAddress(string key)
        {
            ENCODED = key;
        }
    }
}
