﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NemApi;

namespace Tests
{
    [TestClass]
    public class RoundTripBlockChainRequestTests
    {
        [TestMethod]
        public void CanRetrieveLastBlock()
        {
            Connection con = new Connection();

            var blockRequests = new Block(con);

            var response = blockRequests.Last().Result;

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveBlockByHeight()
        {
            Connection con = new Connection();

            var blockRequests = new Block(con);

            var response = blockRequests.ByHeight(400000);

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveBlockByHash()
        {
            Connection con = new Connection();

            var blockRequests = new Block(con);

            var response = blockRequests.ByHash("e5e8999d093cdd640a6b5ac1d5eea4d52eaa052e16fe636f09e37b4affd861f7");

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveChainHeight()
        {
            Connection con = new Connection();

            var blockRequests = new Block(con);

            var response = blockRequests.ChainHeight();

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveChainPart()
        {
            Connection con = new Connection();

            var blockRequests = new Block(con);

            var response = blockRequests.ChainPart(400000);

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveChainScore()
        {
            Connection con = new Connection();

            var blockRequests = new Block(con);

            var response = blockRequests.ChainScore();

            Assert.IsNotNull(response);
        }
    }
}
