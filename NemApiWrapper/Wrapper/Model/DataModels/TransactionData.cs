﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class TransferTransactionData
    {
        public PublicKey multisigAccount { get; set; }
        public EncodedAddress recipient { get; set; }
        public long amount { get; set; }
        public List<Mosaic> listOfMosaics { get; set; }
        public string message { get; set; }
        public bool encrypted { get; set; }
        public int deadline { get; set; }
    }
}
