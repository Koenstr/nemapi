﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class MosaicDefinitionModel
    {        
        internal byte[] NamespaceId { get; set; }
        internal byte[] MosaicName { get; set; }
        internal byte[] Description { get; set; }
        internal List<Property> MosaicProperties { get; set; }
        internal MosaicCreationData Model { get; set; }
        internal MosaicLevy MosaicLevy { get; set; }
        internal int ByteCount { get; set; }

        public MosaicDefinitionModel(MosaicCreationData data)
        {
            this.Model = data;
            this.NamespaceId = Encoding.Default.GetBytes(data.nameSpaceId);
            this.MosaicName = Encoding.Default.GetBytes(data.mosaicName);
            this.Description = Encoding.Default.GetBytes(data.description);
            this.MosaicLevy = data.mosaicLevy;
            
            SetProperties(data.divisibility, data.initialSupply, data.supplyMutable, data.transferable);
            SetBytePropertiesCount();           
        }

        private void SetBytePropertiesCount()
        {
            foreach (Property p in this.MosaicProperties)
            {
                this.ByteCount += p.PropertyLength + 4;
            }
        }

        private void SetProperties(int divisibility, long initialSupply, bool supplyMutable, bool transferable)
        {
            this.MosaicProperties = new List<Property>()
            {
                new Property("divisibility", divisibility.ToString()),
                new Property("initialSupply", initialSupply.ToString()),
                new Property("supplyMutable", supplyMutable == true ? "true" : "false"),
                new Property("transferable", transferable == true ? "true" : "false")
            };
        }
       
        internal class Property
        {
            
            internal int PropertyLength { get; set; }
            internal int PropertyNameLength { get; set; }
            internal string PropertyName { get; set; }
            internal int PropertyValueLength { get; set; }
            internal string PropertyValue { get; set; }

            internal Property(string propertyName, string propertyValue)
            {
                
                this.PropertyName = propertyName;
                this.PropertyValue = propertyValue;
                this.PropertyNameLength = Encoding.Default.GetBytes(PropertyName).Length;
                this.PropertyValueLength = Encoding.Default.GetBytes(propertyValue).Length;
                this.PropertyLength += ByteLength.EightBytes + PropertyNameLength + PropertyValueLength;
            }
        }
    }
}

