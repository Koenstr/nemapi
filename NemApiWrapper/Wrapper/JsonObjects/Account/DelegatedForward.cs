﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class AccountForwarded
    {
        public class Account
        {
            public string address { get; set; }
            public long balance { get; set; }
            public long vestedBalance { get; set; }
            public double importance { get; set; }
            public string publicKey { get; set; }
            public object label { get; set; }
            public int harvestedBlocks { get; set; }
        }

        public class Meta
        {
            public List<object> cosignatoryOf { get; set; }
            public List<object> cosignatories { get; set; }
            public string status { get; set; }
            public string remoteStatus { get; set; }
        }

        public class Data
        {
            public Account account { get; set; }
            public Meta meta { get; set; }
        }
    }
}
