﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace NemApi
{
    public class Block
    {
        private Connection Connection { get; set; }

        public Block(Connection connection)
        {
            Connection = connection;
        }

        public async Task<BlockData.Score> ChainScore()
        {
            return await new AsyncConnector.GetAsync<BlockData.Score>(Connection).get("chain/score");
        }

        public async Task<BlockData.Height> ChainHeight()
        {
            return await new AsyncConnector.GetAsync<BlockData.Height>(Connection).get("/chain/height");
        }

        public async Task<BlockData.Block> ByHash(string hash)
        {
            var path = "/block/get";

            var query = string.Concat("hash=", hash);

            return await new AsyncConnector.GetAsync<BlockData.Block>(Connection).get(path, query);
        }

        public async Task<BlockData.Block> Last()
        {
            return await new AsyncConnector.GetAsync<BlockData.Block>(Connection).get("/chain/last-block");
        }

        public async Task<BlockData.Block> ByHeight(int height)
        {
            return await new AsyncConnector.GetAsync<BlockData.Block>(Connection).get("/block/at/public", new BlockData.Height() { height = height });
        }

        public async Task<BlockData.BlockList> ChainPart(int height)
        {
            return await new AsyncConnector.GetAsync<BlockData.BlockList>(Connection).get("/local/chain/blocks-after", new BlockData.Height(){ height = height });
        }
    }
}
