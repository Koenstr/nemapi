﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    internal class CreateMosaic : Transaction
    {
        private Serializer Serializer = new Serializer();
        private PublicKey Signer { get; set; }
        private MosaicDefinitionModel Definition { get; set; }
        private MosaicLevy Levy { get; set; }
        private Connection Con { get; set; }
        private MosaicCreationData Data { get; set; }
        internal int Length { get; set; }
        private byte[] Bytes { get; set; }
        private byte[] MosaicCreationBytes { get; set; }

        internal CreateMosaic(Connection connection, PublicKey signer, MosaicDefinitionModel data) 
            : base(connection, signer, data.Model.deadline)   
        {
            this.Con = connection;
            this.Signer = signer;
            this.Data = data.Model;
            this.Definition = data;

            setSerializerByteCount();
            serialize();

            this.Bytes = ByteUtils.TruncateByteArray(this.Serializer.GetBytes(), this.Length);

            finalize();
        }

        private void finalize()
        {
            this.UpdateFee(TransactionFee.MosaicDefinitionCreation);
            this.UpdateTransactionType(TransactionType.MosaicDefinitionCreation);
            this.UpdateTransactionVersion(TransactionVersion.VersionOne);

            var tempBytes = new byte[getCommonTransactionBytes().Length + this.Length];

            Array.Copy(getCommonTransactionBytes(), tempBytes, getCommonTransactionBytes().Length);
            Array.Copy(this.Bytes, 0, tempBytes, getCommonTransactionBytes().Length, this.Length);

            this.Length = tempBytes.Length;
            this.MosaicCreationBytes = tempBytes;
        }

        internal byte[] getBytes()
        {
            return this.MosaicCreationBytes;
        }

        private void setSerializerByteCount()
        {
            this.Length += 116 
                         + this.Definition.NamespaceId.Length 
                         + this.Definition.MosaicName.Length 
                         + this.Definition.Description.Length
                         + this.Definition.ByteCount;
        }
       
        private void serialize()
        {
            this.Serializer.WriteInt(60 + this.Definition.NamespaceId.Length
                         + this.Definition.MosaicName.Length
                         + this.Definition.Description.Length + this.Definition.ByteCount);

            this.Serializer.WriteInt(ByteLength.PublicKeyLength);
            this.Serializer.WriteBytes(CryptoBytes.FromHexString(Signer.RAW));
            this.Serializer.WriteInt(ByteLength.EightBytes 
                                    + Definition.NamespaceId.Length 
                                    + Definition.MosaicName.Length);

            this.Serializer.WriteInt(Definition.NamespaceId.Length);
            this.Serializer.WriteBytes(Definition.NamespaceId);
            this.Serializer.WriteInt(Definition.MosaicName.Length);
            this.Serializer.WriteBytes(Definition.MosaicName);
            this.Serializer.WriteInt(Definition.Description.Length);
            this.Serializer.WriteBytes(Definition.Description);
            this.Serializer.WriteInt(Definition.MosaicProperties.Count);

            foreach (MosaicDefinitionModel.Property property in this.Definition.MosaicProperties)
            {
                this.Serializer.WriteInt(property.PropertyLength);
                this.Serializer.WriteInt(property.PropertyNameLength);
                this.Serializer.WriteString(property.PropertyName);
                this.Serializer.WriteInt(property.PropertyValueLength);
                this.Serializer.WriteString(property.PropertyValue);           
            }

            if (Definition.MosaicLevy != null)
            {
                this.Definition.MosaicLevy.SerializeLevy(this.Serializer);
                this.Serializer.UpdateNthFourBytes(60, this.Definition.ByteCount + Levy.Length);
                this.Length += Levy.Length;
            }
            else Serializer.WriteInt(DefaultValues.ZeroValuePlaceHolder);

            this.Serializer.WriteInt(ByteLength.AddressLength);
            this.Serializer.WriteString(AddressEncoding.ToEncoded(this.Con.getNetworkVersion(), new PublicKey(DefaultValues.MainNetCreationFeeSink)));
            this.Serializer.WriteLong(Fee.Creation);
        }
    }
}
