﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NemApi;
using Chaos.NaCl;
using System.Collections.Generic;
using System.Linq;

namespace Tests
{
    [TestClass]
    public class Crypto
    {
        [TestMethod]
        public void CanConvertPrivateKeyToPublicKey()
        {
            string privateKey = "abf4cf55a2b3f742d7543d9cc17f50447b969e6e06f5ea9195d428ab12b7318d";

            string expected   = "8a558c728c21c126181e5e654b404a45b4f0137ce88177435a69978cc6bec1f4";

            string result = new PublicKey(new PrivateKey(privateKey)).RAW;
           
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CanConvertPublicKeyToMainNetAddress()
        {
            string privateKey = "abf4cf55a2b3f742d7543d9cc17f50447b969e6e06f5ea9195d428ab12b7318d";

            string result = AddressEncoding.ToEncoded(0x68, new PublicKey(new PrivateKey(privateKey)));

            string expected = "NAZQOWGZJ5PKR3QJEUZEMS6MXQX3WZZZAKKTLPZT";

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CanConvertPublicKeyToTestNetAddress()
        {


            string privateKey = "abf4cf55a2b3f742d7543d9cc17f50447b969e6e06f5ea9195d428ab12b7318d";

            string result = AddressEncoding.ToEncoded(0x98, new PublicKey(new PrivateKey(privateKey)));

            string expected = "TAZQOWGZJ5PKR3QJEUZEMS6MXQX3WZZZAJJDHP3H";

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CanVerifySignature()
        {
            byte[] sig = new byte[64];
            byte[] sk = new byte[64];

            string privateKey = "abf4cf55a2b3f742d7543d9cc17f50447b969e6e06f5ea9195d428ab12b7318d";
            string publicKey = "8a558c728c21c126181e5e654b404a45b4f0137ce88177435a69978cc6bec1f4";
            string expected = "d9cec0cc0e3465fab229f8e1d6db68ab9cc99a18cb0435f70deb6100948576cd5c0aa1feb550bdd8693ef81eb10a556a622db1f9301986827b96716a7134230c";
            byte[] data = CryptoBytes.FromHexString("8ce03cd60514233b86789729102ea09e867fc6d964dea8c2018ef7d0a2e0e24bf7e348e917116690b9");
            Array.Copy(CryptoBytes.FromHexString(privateKey), sk, 32);
            Array.Copy(GetKeyBytes(publicKey), 0, sk, 32, 32);

            Ed25519.crypto_sign2(sig, data, sk, 32);
            Assert.IsTrue( Ed25519.Verify(sig, data, GetKeyBytes(publicKey)));
            Assert.AreEqual(expected, CryptoBytes.ToHexStringLower(sig));
        }

        internal byte[] GetKeyBytes(string RAW)
        {
            IEnumerable<string> set = Split(RAW, 2);
            byte[] bytes = new byte[32];
            int y = 0;
            foreach (string s in set)
            {
                int x = int.Parse(s, System.Globalization.NumberStyles.HexNumber);
                bytes[y] = (byte)x;
                y++;
            }

            return bytes;
        }

        private static IEnumerable<string> Split(string str, int chunkSize)
        {
            return Enumerable.Range(0, str.Length / chunkSize)
                .Select(i => str.Substring(i * chunkSize, chunkSize));
        }
    }
}
