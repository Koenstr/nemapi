﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.NetworkInformation;
using NemApi;

namespace Tests
{
    [TestClass]
    public class ConnectionTest
    {
        [TestMethod]
        public void CanReachAutoConnectedHost()
        {
            Connection con = new Connection();

            Assert.IsTrue(PingHost(con.getHost()));
        }

        public static bool PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                pingable = false;
            }
            return pingable;
        }

    }
}
