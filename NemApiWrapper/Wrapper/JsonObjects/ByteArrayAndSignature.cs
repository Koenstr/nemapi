﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    /*
    * Stores stores the transaction byte array and signature data to be prepared amd sent to nis.
    */
    public class ByteArrayWtihSignature
    {
        public string data { get; set; }
        public string signature { get; set; }
    }
}
