﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class Transactions
    {
        public class Meta
        {
            public string data { get; set; }
        }

        public class TransactionMetaData
        {
            public int id { get; set; }
            public int height { get; set; }
            public Meta hash { get; set; }
        }

        public class Message
        {
            public string payload { get; set; }
            public int type { get; set; }
        }

        public class OtherTrans
        {
            public int timeStamp { get; set; }
            public long amount { get; set; }
            public int fee { get; set; }
            public string recipient { get; set; }
            public int type { get; set; }
            public int deadline { get; set; }
            public Message message { get; set; }
            public int version { get; set; }
            public string signer { get; set; }
        }
        public class Transaction
        {
            public int timeStamp { get; set; }
            public string signature { get; set; }
            public int fee { get; set; }
            public int type { get; set; }
            public int deadline { get; set; }
            public int version { get; set; }
            public List<object> signatures { get; set; }
            public string signer { get; set; }
            public OtherTrans otherTrans { get; set; }
        }
        public class UnconfirmedTransactionData
        {
            public Meta meta { get; set; }
            public Transaction Transaction { get; set; }
        }
        public class TransactionData
        {
            public TransactionMetaData meta { get; set; }
            public Transaction Transaction { get; set; }
        }

        public class List
        {
            public List<TransactionData> data { get; set; }
        }
        public class UnconfirmedList
        {
            public List<UnconfirmedTransactionData> data { get; set; }
        }
    }
}
