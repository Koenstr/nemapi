﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    internal class SupplyChange : Transaction
    { 
        private Serializer Serializer = new Serializer();
        private MosaicSupplyChangeData Data { get; set; }
        private byte[] SupplyChangeBytes { get; set; }
        private byte[] Bytes { get; set; }
        private byte[] NameSpaceId { get; set; }
        private byte[] MosaicName { get; set; }
        internal int Length { get; set; }

        internal SupplyChange(Connection con, PublicKey sender, MosaicSupplyChangeData data) 
            : base(con, sender, data.deadline)
        {
            this.Data = data;
            this.NameSpaceId = Encoding.Default.GetBytes(data.nameSpaceId);
            this.MosaicName = Encoding.Default.GetBytes(data.mosaicName);
            this.Length = 24 + this.NameSpaceId.Length + this.MosaicName.Length;

            serialize();

            this.Bytes = ByteUtils.TruncateByteArray(Serializer.GetBytes(), this.Length);
            finalize();
        }

        private void serialize()
        {
            this.Serializer.WriteInt(ByteLength.EightBytes + this.NameSpaceId.Length + this.MosaicName.Length);
            
            this.Serializer.WriteInt(this.NameSpaceId.Length);

            this.Serializer.WriteBytes(this.NameSpaceId);

            this.Serializer.WriteInt(this.MosaicName.Length);

            this.Serializer.WriteBytes(this.MosaicName);

            this.Serializer.WriteInt(this.Data.supplyChangeType);

            this.Serializer.WriteLong(this.Data.delta);
        }

        private void finalize()
        {
            UpdateFee(TransactionFee.SupplyChange);
            UpdateTransactionType(TransactionType.MosiacSupplyChange);
            UpdateTransactionVersion(TransactionVersion.VersionOne);

            var tempBytes = new byte[getCommonTransactionBytes().Length + this.Length];

            Array.Copy(getCommonTransactionBytes(), tempBytes, getCommonTransactionBytes().Length);
            Array.Copy(this.Bytes, 0, tempBytes, getCommonTransactionBytes().Length, this.Length);

            this.Length = tempBytes.Length;
            this.SupplyChangeBytes = tempBytes;
        }

        internal byte[] getBytes()
        {
            return this.SupplyChangeBytes;
        }
    }
    
}
