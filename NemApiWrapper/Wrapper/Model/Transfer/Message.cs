﻿using Chaos.NaCl;
using System;
using System.Text;

namespace NemApi
{
    /*
    * Creates/prepares a message object to be serialized
    *
    */
    internal class Message
    {
        internal int Length { get; set; }
        private string MessageString { get; set; }
        private byte[] MessageBytes { get; set; }
        private int MessageFieldLengthInBytes { get; set; }
        private bool Encrypted { get; set; }
        private int PayloadLengthInBytes { get; set; }
        private long Fee { get; set; }
        internal Serializer Serializer = new Serializer();
        VerifiableAccount Sender { get; set; }
        UnverifiableAccount Recipient { get; set; }


        /*
        * Constructs the message object and initiates its serialization
        *
        * @Param: Serializer, The serializer to use during serialization
        * @Param: Message, The message string. Note: if null, a zero value byte[4] is serialized instead
        * @Param: Encrypted, Whether the message should be encrypted or not
        */
        internal Message(Connection con, PrivateKey senderKey, string recipient, string message, bool encrypted)

        {
            this.Encrypted = encrypted;
            this.MessageString = message;

            if (MessageString != null)
            {
                if (Encrypted)
                {
                    this.Sender = new VerifiableAccount(con, senderKey);

                    this.Recipient = new UnverifiableAccount(con, new EncodedAddress(recipient));

                    this.MessageBytes = new Ed25519BlockCipher(this.Sender, this.Recipient).Encrypt(Encoding.UTF8.GetBytes(MessageString));
                }
                else this.MessageBytes = Encoding.UTF8.GetBytes(MessageString);

                this.MessageFieldLengthInBytes = this.MessageBytes.Length + ByteLength.EightBytes;

                this.PayloadLengthInBytes = this.MessageBytes.Length;
            }

            this.serialize();
            this.calculateMessageFee();

        }
        private void calculateMessageFee()
        {
            this.Fee += this.Encrypted == true 
                ? 6000000 + (MessageString.Length / 32 + 1000000) 
                          : (MessageString.Length / 32 + 1000000);          
        }

        internal long getFee()
        {
            return this.Fee;
        }

        internal byte[] getMessageBytes()
        {
            return this.MessageBytes;
        }

        private void serialize()
        {
            if (this.MessageBytes != null)
            {
                this.Serializer.WriteInt(this.MessageBytes.Length + ByteLength.EightBytes);
                this.Serializer.WriteInt(Encrypted ? 2 : 1);
                this.Serializer.WriteInt(this.MessageBytes.Length);
                this.Serializer.WriteBytes(this.MessageBytes);
                this.MessageBytes = ByteUtils.TruncateByteArray(this.Serializer.GetBytes(), this.PayloadLengthInBytes + 12);
                this.Length = StructureLength.MessageStructure + PayloadLengthInBytes;
            }
            else {
                this.MessageBytes = DefaultBytes.ZeroByteValue;
                this.Length = ByteLength.ZERO;
            }
        }
    }
}