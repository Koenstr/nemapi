﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class MosaicLevy
    {
        internal EncodedAddress FeeBeneficiary { get; set; }
        internal Mosaic Mosaic { get; set; }
        internal int FeeType { get; set; }
        internal int Length { get; set; }

        public MosaicLevy(
            EncodedAddress feeBeneficiary, 
            Mosaic mosaic, 
            int feeType)
        {
            if (feeType != 1 && feeType != 2)
                throw new ArgumentException("invalid fee");
            
            this.FeeBeneficiary = feeBeneficiary;
            this.FeeType = feeType == 1 ? Fee.Absolute : Fee.Percentile;
            this.Mosaic = mosaic;
            this.Length = StructureLength.MosaicLevy
                        + StructureLength.LevyStructureBytes
                        + this.Mosaic.LengthOfNameSpaceId
                        + this.Mosaic.LengthOfMosaicName;           
        }

        internal void SerializeLevy(Serializer Serializer)
        {
            // length of levy structure
            Serializer.WriteInt(this.Length);

            // fee type
            Serializer.WriteInt(this.FeeType);

            // length of recipient address
            Serializer.WriteInt(ByteLength.AddressLength);

            // address of beneficiary
            Serializer.WriteString(this.FeeBeneficiary.ENCODED);

            // length of mosaic id structure
            Serializer.WriteInt(this.Mosaic.LengthOfMosaicIdStructure);

            // length of name space
            Serializer.WriteInt(this.Mosaic.LengthOfNameSpaceId);

            // name space
            Serializer.WriteString(this.Mosaic.NameSpaceId);

            // length of mosaic name
            Serializer.WriteInt(this.Mosaic.LengthOfMosaicName);

            // mosaic name
            Serializer.WriteString(this.Mosaic.MosaicName);

            // fee amount
            Serializer.WriteLong(this.Mosaic.Quantity);
        }
    }
}
