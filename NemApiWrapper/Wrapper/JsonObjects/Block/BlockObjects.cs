﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    /*
    * Block related data
    */
    public class BlockData
    {

        public class PrevBlockHash
        {
            public string data { get; set; }
        }

        public class Block
        {
            public int timeStamp { get; set; }
            public string signature { get; set; }
            public PrevBlockHash prevBlockHash { get; set; }
            public int type { get; set; }
            public List<TransactionDatum> transactions { get; set; }
            public int version { get; set; }
            public string signer { get; set; }
            public int height { get; set; }
        }

        public class BlockDatum
        {
            public long difficulty { get; set; }
            public List<ExplorerTransferViewModel> txes { get; set; }
            public Block block { get; set; }
            public string hash { get; set; }
        }

        public class BlockList
        {
            public List<BlockDatum> data { get; set; }
        }

        public class Height
        {
            public int height { get; set; }
        }

        public class Hash
        {
            public string data { get; set; }
        }

        public class Meta
        {
            public int id { get; set; }
            public int height { get; set; }
            public Hash hash { get; set; }
        }

        /*
        * Block transaction related data
        */

        public class Score
        {
            public string score { get; set; }
        }

        public class Message
        {
            public string payload { get; set; }
            public int type { get; set; }
        }

        public class Transaction
        {
            public int timeStamp { get; set; }
            public long amount { get; set; }
            public string signature { get; set; }
            public int fee { get; set; }
            public string recipient { get; set; }
            public int type { get; set; }
            public int deadline { get; set; }
            public Message message { get; set; }
            public int version { get; set; }
            public string signer { get; set; }
        }

        public class TransactionDatum
        {
            public Meta meta { get; set; }
            public Transaction transaction { get; set; }
        }

        public class ExplorerTransferViewModel
        {
            public TransactionDatum transaction { get; set; }
            public string hash { get; set; }
            public string innerHash { get; set; }
        }
    }
}
   
