﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    internal class ProvisionNamespace : Transaction
    {
        private Serializer Serializer = new Serializer();
        private ProvisionNameSpaceData Data { get; set; }
        private Connection Con { get; set; }
        private int LengthOfNewPart { get; set; }
        private int LengthOfParent { get; set; }
        private byte[] Bytes { get; set; }
        internal int Length { get; set; }

        internal ProvisionNamespace(Connection connection, PublicKey signer, ProvisionNameSpaceData data)
            : base(connection, signer, data.deadline)
        {
            this.Con = connection;
            this.Data = data;
            if (this.Data.parent == null) this.Data.parent = DefaultValues.EmptyString;
            this.LengthOfNewPart = Encoding.Default.GetBytes(Data.newPart).Length;
            this.LengthOfParent = Encoding.Default.GetBytes(Data.parent).Length;

            this.Serialize();

            this.Bytes = ByteUtils.TruncateByteArray(Serializer.GetBytes(), this.Length);
            finalize();
        }

        private void finalize()
        {
            this.UpdateFee(TransactionFee.ProvisionNameSpace);
            this.UpdateTransactionType(TransactionType.ProvisionNamespace);
            this.UpdateTransactionVersion(TransactionVersion.VersionOne);

            var tempBytes = new byte[getCommonTransactionBytes().Length + this.Length];

            Array.Copy(getCommonTransactionBytes(), tempBytes, getCommonTransactionBytes().Length);
            Array.Copy(this.Bytes, 0, tempBytes, getCommonTransactionBytes().Length, this.Length);

            this.Length = tempBytes.Length;
            this.Bytes = tempBytes;
        }

        internal byte[] getBytes()
        {
            return this.Bytes;
        }

        private void Serialize()
        {
            this.Length += StructureLength.ProvisionNameSpace + this.LengthOfNewPart;
            this.Serializer.WriteInt(ByteLength.AddressLength);
            this.Serializer.WriteString(AddressEncoding.ToEncoded(this.Con.getNetworkVersion(), new PublicKey(DefaultValues.MainNetRentalFeeSinkPublicKey)));
            this.Serializer.WriteLong(this.Data.parent == DefaultValues.EmptyString ? Fee.Rental : Fee.SubSpaceRental);
            this.Serializer.WriteInt(this.LengthOfNewPart);
            this.Serializer.WriteBytes(Encoding.Default.GetBytes(this.Data.newPart));

            if (string.Empty != this.Data.parent)
            {
                this.Length += ByteLength.FourBytes + LengthOfParent;
                this.Serializer.WriteInt(LengthOfParent);
                this.Serializer.WriteBytes(Encoding.Default.GetBytes(this.Data.parent));
            }
            else
            {
                this.Length += ByteLength.FourBytes;
                this.Serializer.WriteBytes(DefaultBytes.MaxByteValue);
            }
        }
    }
}