﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class MosaicsOwned
    {
        public class MosaicId
        {
            public string namespaceId { get; set; }
            public string name { get; set; }
        }

        public class Mosaic
        {
            public MosaicId mosaicId { get; set; }
            public long quantity { get; set; }
        }

        public class RootObject
        {
            public List<Mosaic> data { get; set; }
        }

    }
}
