﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class Importances
    {
        public class Importance
        {
            public int isSet { get; set; }
            public double score { get; set; }
            public double ev { get; set; }
            public long height { get; set; }
        }

        public class Datum
        {
            public string address { get; set; }
            public Importance importance { get; set; }
        }

        public class ListImportances
        {
            public List<Datum> data { get; set; }
        }
    }
}
