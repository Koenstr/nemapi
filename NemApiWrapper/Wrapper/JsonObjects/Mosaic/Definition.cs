﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class Definition
    {
        public class Meta
        {
            public int id { get; set; }
        }

        public class Id
        {
            public string namespaceId { get; set; }
            public string name { get; set; }
        }

        public class Property
        {
            public string name { get; set; }
            public string value { get; set; }
        }

        public class Levy
        {
            public int type { get; set; }
            public string recipient { get; set; }
            public Id mosaicId { get; set; }
            public int fee { get; set; }
        }

        public class Mosaic
        {
            public string creator { get; set; }
            public string description { get; set; }
            public Id id { get; set; }
            public List<Property> properties { get; set; }
            public Levy levy { get; set; }
        }

        public class Datum
        {
            public Meta meta { get; set; }
            public Mosaic mosaic { get; set; }
        }

        public class List
        {
            public List<Datum> data { get; set; }
        }
    }

    
}
