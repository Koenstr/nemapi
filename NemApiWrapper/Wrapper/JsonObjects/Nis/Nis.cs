﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class NetworkTime
    {
        public long sendTimeStamp { get; set; }
        public long receiveTimeStamp { get; set; }
    }

    public class HeartBeat
    {
        public int code { get; set; }
        public int type { get; set; }
        public string message { get; set; }
    }

    public class Status
    {
        public int code { get; set; }
        public int type { get; set; }
        public string message { get; set; }
    }

    public class Datum
    {
        public string dateTime { get; set; }
        public int currentTimeOffset { get; set; }
        public int change { get; set; }
    }

    public class TimeSync
    {
        public List<Datum> data { get; set; }
    }
}
