﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class PublicKey
    {
        public string RAW { get; private set; }

        public PublicKey(string key)
        {
            RAW = key;
        }

        public PublicKey(PrivateKey key)
        {
            RAW = CryptoBytes.ToHexStringLower(PublicKeyConversion.ToPublicKey(key));
        }
    }

    
}
