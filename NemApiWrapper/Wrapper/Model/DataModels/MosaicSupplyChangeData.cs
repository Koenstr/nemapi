﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class MosaicSupplyChangeData
    {
        public PublicKey multisigAccount { get; set; }
        public int deadline { get; set; }
        public string nameSpaceId { get; set; }
        public string mosaicName { get; set; }
        public int supplyChangeType { get; set; }
        public int delta { get; set; }
    }
}
