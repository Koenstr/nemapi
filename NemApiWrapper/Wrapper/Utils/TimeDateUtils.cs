﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    internal static class TimeDateUtils
    {
        public static int EpochTimeInMilliSeconds()
        {
            TimeSpan timeSpan = DateTime.UtcNow - new DateTime(2015, 03, 29, 0, 6, 25, 0);
            int secondsSinceEpoch = (int)timeSpan.TotalSeconds;
            return secondsSinceEpoch;
        }
    }
}
