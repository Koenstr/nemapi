﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class MosaicDefinitions
    {
        public class Id
        {
            public string namespaceId { get; set; }
            public string name { get; set; }
        }

        public class Property
        {
            public string name { get; set; }
            public string value { get; set; }
        }

        public class Mosaic
        {
            public string creator { get; set; }
            public Id id { get; set; }
            public string description { get; set; }
            public List<Property> properties { get; set; }
        }

        public class List
        {
            public List<Mosaic> data { get; set; }
        }
    }
}
