﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
  
    public class NameSpace
    {
        public string fqn { get; set; }
        public string owner { get; set; }
        public long height { get; set; }
    }

    public class NameSpaceList
    {
        public List<NameSpace> data { get; set; }
    }
    
}
