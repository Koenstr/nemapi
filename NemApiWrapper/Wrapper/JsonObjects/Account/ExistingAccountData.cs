﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class ExistingAccount
    {
        public class Cosignatory
        {
            public string address { get; set; }
            public int harvestedBlocks { get; set; }
            public long balance { get; set; }
            public double importance { get; set; }
            public long vestedBalance { get; set; }
            public string publicKey { get; set; }
            public object label { get; set; }
            public MultisigInfo multisigInfo { get; set; }
        }

        public class MultisigInfo
        {
            public int cosignatoriesCount { get; set; }
            public int minCosignatories { get; set; }
        }

        public class MultisigInfo2
        {
            public int cosignatoriesCount { get; set; }
            public int minCosignatories { get; set; }
        }

        public class CosignatoryOf
        {
            public string address { get; set; }
            public int harvestedBlocks { get; set; }
            public long balance { get; set; }
            public double importance { get; set; }
            public long vestedBalance { get; set; }
            public string publicKey { get; set; }
            public object label { get; set; }
            public MultisigInfo multisigInfo { get; set; }
        }
        public class Account
        {
            public string address { get; set; }
            public int harvestedBlocks { get; set; }
            public long balance { get; set; }
            public double importance { get; set; }
            public long vestedBalance { get; set; }
           
            public string publicKey { get; set; }
            public object label { get; set; }
            public MultisigInfo2 multisigInfo2 { get; set; }
        }

        public class Meta
        {
            public List<Cosignatory> cosignatories { get; set; }
            public List<CosignatoryOf> cosignatoryOf { get; set; } 
            public string status { get; set; }
            public string remoteStatus { get; set; }
        }

        public class Data
        {
            public Meta meta { get; set; }
            public Account account { get; set; } 
        }
    }
}
