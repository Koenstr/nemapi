﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NemApi;

namespace Tests
{
    [TestClass]
    public class UnverifiableAccountRequests
    {
        [TestMethod]
        public void CanRetrieveAccountInfo()
        {
            Connection con = new Connection();
            PublicKey key = new PublicKey(TestConstants.pubKey);
            UnverifiableAccount account = new AccountFactory(con).fromPublicKey(key);

            var response = account.getAccountInfoAsync().Result;

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveAllTransactions()
        {
            Connection con = new Connection();
            PublicKey key = new PublicKey(TestConstants.pubKey);
            UnverifiableAccount account = new AccountFactory(con).fromPublicKey(key);

            var response = account.getAllTransactionsAsync().Result;
            Assert.IsTrue(response.data.Count > 0);
        }

        [TestMethod]
        public void CanRetrieveUnconfirmedTransactions()
        {
            Connection con = new Connection();
            PublicKey key = new PublicKey(TestConstants.pubKey);
            UnverifiableAccount account = new AccountFactory(con).fromPublicKey(key);

            var response = account.getUnconfirmedTransactionsAsync().Result;

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveOutGoingTransactions()
        {
            Connection con = new Connection();
            PublicKey key = new PublicKey(TestConstants.pubKey);
            UnverifiableAccount account = new AccountFactory(con).fromPublicKey(key);

            var response = account.getOutgoingTransactionsAsync().Result;

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveIncomingTransactions()
        {
            Connection con = new Connection();
            PublicKey key = new PublicKey(TestConstants.pubKey);
            UnverifiableAccount account = new AccountFactory(con).fromPublicKey(key);

            var response = account.getIncomingTransactionsAsync().Result;

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveAccountStatus()
        {
            Connection con = new Connection();
            PublicKey key = new PublicKey(TestConstants.pubKey);
            UnverifiableAccount account = new AccountFactory(con).fromPublicKey(key);

            var response = account.getAccountStatusAsync().Result;

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveDelegatedAccountRoot()
        {
            Connection con = new Connection();
            PublicKey key = new PublicKey(TestConstants.pubKey);
            UnverifiableAccount account = new AccountFactory(con).fromPublicKey(key);

            var response = account.getDelegatedAccountRootAsync().Result;

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveHarvestingInfo()
        {
            Connection con = new Connection();
            PublicKey key = new PublicKey(TestConstants.pubKey);
            UnverifiableAccount account = new AccountFactory(con).fromPublicKey(key);

            var response = account.getHarvestingInfoAsync().Result;

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveMosaicDefinition()
        {
            Connection con = new Connection();
            PublicKey key = new PublicKey(TestConstants.pubKey);
            UnverifiableAccount account = new AccountFactory(con).fromPublicKey(key);

            var response = account.getMosaicsAsync().Result;

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveMosaicOwned()
        {
            Connection con = new Connection();
            PublicKey key = new PublicKey(TestConstants.pubKey);
            UnverifiableAccount account = new AccountFactory(con).fromPublicKey(key);

            var response = account.getMosaicsOwnedAsync().Result;

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveNameSpaceMosaicDefinition()
        {
            Connection con = new Connection();
            PublicKey key = new PublicKey(TestConstants.pubKey);
            UnverifiableAccount account = new AccountFactory(con).fromPublicKey(key);

            var response = account.getMosaicsByNameSpaceAsync("jabo38").Result;
            
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveNameSpaces()
        {
            Connection con = new Connection();
            PublicKey key = new PublicKey(TestConstants.pubKey);
            UnverifiableAccount account = new AccountFactory(con).fromPublicKey(key);

            var response = account.getNamespacesAsync().Result;

            Assert.IsNotNull(response);
        }

        


    }
}
