﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    /*
    * Contains sets of default/constant values.
    */
    
    internal static class TransactionType
    {
        internal const int TransferTransaction = 0x0101;
        internal const int ImportanceTransfer = 0x0801;
        internal const int MultisigAggregateModification = 0x1001;
        internal const int MultisigSignatureTransaction = 0x1002;
        internal const int MultisigTransaction = 0x1004;
        internal const int ProvisionNamespace = 0x2001;
        internal const int MosaicDefinitionCreation = 0x4001;
        internal const int MosiacSupplyChange = 0x4002;

    }
    internal static class TransactionVersion
    {
        internal const int VersionOne = 0x01;
        internal const int VersionTwo = 0x02;
    }
    internal static class StructureLength
    {
        internal const int MultisigSignature = 0x54;
        internal const int MessageStructure = 0x08;
        internal const int AggregateModification = 0x28;
        internal const int RelativeChange = 0x04;
        internal const int TransactionCommon = 0x3c;
        internal const int MultiSigHeader = 0x40;
        internal const int TransferTransaction = 0x3c;
        internal const int ProvisionNameSpace = 0x38;
        internal const int ImportnaceTransfer = 0x28;
        internal const int TransactionHash = 0x24;
        internal const int MosaicObject = 0x14;
        internal const int NoOfPropertyBytes = 0x04;
        internal const int LevyStructureBytes = 0x08;
        internal const int CreationFeeSinkBytesAfterLevy = 0x2c;
        internal const int MosaicLevy = 0x3c;
    }

    internal static class ByteLength
    {
        internal const int PublicKeyLength = 0x20;
        internal const int HashLength = 0x20;
        internal const int AddressLength = 0x28;
        internal const int FourBytes = 0x04;
        internal const int EightBytes = 0x08;
        internal const int ZERO = 0x00;
        
    }

    internal static class DefaultBytes
    {
        internal static byte[] MaxByteValue = new byte[]{  0xff, 0xff, 0xff, 0xff };
        internal static byte[] ZeroByteValue = new byte[] { 0x00, 0x00, 0x00, 0x00 };
        internal static byte[] Activate = new byte[] { 0x01, 0x00, 0x00, 0x00 };
        internal static byte[] Deactivate = new byte[] { 0x02, 0x00, 0x00, 0x00 };

    }
    internal static class DefaultValues
    {
        internal const string MainNetRentalFeeSinkPublicKey = "3e82e1c1e4a75adaa3cba8c101c3cd31d9817a2eb966eb3b511fb2ed45b8e262";
        internal const string MainNetCreationFeeSink = "53e140b5947f104cabc2d6fe8baedbc30ef9a0609c717d9613de593ec2a266d3";
        internal const string EmptyString = "";
        internal const int ZeroValuePlaceHolder = 0;
    }
    internal static class Fee
    {
        internal const long SubSpaceRental = 0x12A05F200;
        internal const long Rental = 0xBA43B7400;
        internal const long Creation = 0xBA43B7400;
        internal const int Absolute = 0x01;
        internal const int Percentile = 0x02;
    }
    internal static class TransactionFee
    {
        internal const int SupplyChange = 0x66FF300;
        internal const int ProvisionNameSpace = 0x1312D00;
        internal const int MosaicDefinitionCreation = 0x1312D00;

        internal const int MultisigSignature = 0x5B8D80;
        internal const int MultisigWrapper = 0x5B8D80;
        internal const int ImportanceTransfer = 0x5B8D80;
    }
    public class TestConstants
    {
        public const string address = "TCEIV52VWTGUTMYOXYPVTGMGBMQC77EH4MBJRSNT";
        public const string pubKey = "09ac855e55fad630bdfbd52e08c54e520524e6f9bbd14844a2b0ecca66cae6a0";
        public const string privKey = "fcdadb68356c6227a0942b377209401574ece844e8e579edbfe36a5193cf8cb5";

        public static List<PublicKey> listOfPublicKeys = new List<PublicKey>()
        {
            new PublicKey("c559463bf86320eeac6c846a124cde5e6f457108c1d852e54ea55611d1e545bb"),
            new PublicKey("72d0e65f1ede79c4af0ba7ec14204e10f0f7ea09f2bc43259cd60ea8c3a087e2"),
            new PublicKey("3ec8923f9ea5ea14f8aaa7e7c2784653ed8c7de44e352ef9fc1dee81fc3fa1a3")
        };
    }

}
