﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NemApi;

namespace Tests.NisRequests
{
    [TestClass]
    public class NisRequestTests
    {
        [TestMethod]
        public void CanRetrieveHeartBeat()
        {
            Connection con = new Connection();

            var nisRequests = new Nis(con);

            var response = nisRequests.HeartBeat();

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveNetworkTime()
        {
            Connection con = new Connection();

            var nisRequests = new Nis(con);

            var response = nisRequests.NetworkTime();

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveStatus()
        {
            Connection con = new Connection();

            var nisRequests = new Nis(con);

            var response = nisRequests.Status();

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void CanRetrieveTimeSync()
        {
            Connection con = new Connection();

            var nisRequests = new Nis(con);

            var response = nisRequests.TimeSync();

            Assert.IsNotNull(response);
        }
    }
}
