﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NemApi;

namespace Tests
{
    [TestClass]
    public class AccountTest
    {
        [TestMethod]
        public void CanCreateVerifiableAccount()
        {
            Connection con = new Connection();
            con.setTestNet();

            PrivateKey key = new PrivateKey(TestConstants.privKey);

            VerifiableAccount account = new VerifiableAccount(con, key);
            Assert.AreEqual(TestConstants.address, account.Encoded.ENCODED);
            Assert.AreEqual(TestConstants.pubKey, account.PublicKey.RAW);
            Assert.AreEqual(TestConstants.privKey, StringUtils.ConvertToUnsecureString( account.PrivateKey.RAW));
        }

        [TestMethod]
        public void CanCreateUnverifiableAccountFromPublicKey()
        {
            Connection con = new Connection();
            con.setTestNet();

            PublicKey key = new PublicKey(TestConstants.pubKey);

            UnverifiableAccount account = new UnverifiableAccount(con, key);

            Assert.AreEqual(TestConstants.address, account.Encoded.ENCODED);
            Assert.AreEqual(TestConstants.pubKey, account.PublicKey.RAW);
        }

        [TestMethod]
        public void CanCreateUnverifiableAccountFromPublicKeyWithAccountFactory()
        {
            Connection con = new Connection();
            con.setTestNet();

            PublicKey key = new PublicKey(TestConstants.pubKey);

            UnverifiableAccount account = new AccountFactory(con).fromPublicKey(key);
            UnverifiableAccount account2 = new AccountFactory(con).fromPublicKey(TestConstants.pubKey);

            Assert.AreEqual(TestConstants.address, account.Encoded.ENCODED);
            Assert.AreEqual(TestConstants.pubKey, account.PublicKey.RAW);

            Assert.AreEqual(TestConstants.address, account2.Encoded.ENCODED);
            Assert.AreEqual(TestConstants.pubKey, account2.PublicKey.RAW);
        }

        [TestMethod]
        public void CanCreateVerifiableAccountFromPrivateKeyWithAccountFactory()
        {
            Connection con = new Connection();
            con.setTestNet();

            PrivateKey key = new PrivateKey(TestConstants.privKey);

            VerifiableAccount account = new AccountFactory(con).fromPrivateKey(key);

            Assert.AreEqual(TestConstants.address, account.Encoded.ENCODED);
            Assert.AreEqual(TestConstants.pubKey, account.PublicKey.RAW);
            Assert.AreEqual(TestConstants.privKey, StringUtils.ConvertToUnsecureString(account.PrivateKey.RAW));

            VerifiableAccount account2 = new AccountFactory(con).fromPrivateKey(TestConstants.privKey);

            Assert.AreEqual(TestConstants.address, account2.Encoded.ENCODED);
            Assert.AreEqual(TestConstants.pubKey, account2.PublicKey.RAW);
            Assert.AreEqual(TestConstants.privKey, StringUtils.ConvertToUnsecureString(account2.PrivateKey.RAW));
        }
    }
}
