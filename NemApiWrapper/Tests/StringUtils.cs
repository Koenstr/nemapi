﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NemApi;
using System.Security;

namespace Tests
{
    [TestClass]
    public class StringUtilsTests
    {
        [TestMethod]
        public void CanConvertToAndFromSecureString()
        {
            var privateKey = "abf4cf55a2b3f742d7543d9cc17f50447b969e6e06f5ea9195d428ab12b7318d";

            var expected = "abf4cf55a2b3f742d7543d9cc17f50447b969e6e06f5ea9195d428ab12b7318d";

            var secureKey = StringUtils.ToSecureString(privateKey);

            var result = StringUtils.ConvertToUnsecureString(secureKey);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CanConvertAddressToPrettyAddress()
        {
            string address = "NAUARLU4RMH2CW2UFWAIDAD73C5JYYSZ7ISSDYME";
            string result = StringUtils.GetResultsWithHyphen(address);
            string expected = "NAUARL-U4RMH2-CW2UFW-AIDAD7-3C5JYY-SZ7ISS-DYME";
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CanConvertPrettyAddressToAddress()
        {
            string address = "NAUARL-U4RMH2-CW2UFW-AIDAD7-3C5JYY-SZ7ISS-DYME";
            string result = StringUtils.GetResultsWithoutHyphen(address);
            string expected = "NAUARLU4RMH2CW2UFWAIDAD73C5JYYSZ7ISSDYME";
            Assert.AreEqual(expected, result);
        }
    }
}
