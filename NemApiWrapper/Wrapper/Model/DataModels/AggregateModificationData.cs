﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class AggregateModificationData
    {
        public List<AggregateModification> modifications { get; set; }
        public PublicKey multisigAccount { get; set; }
        public int relativeChange { get; set; }
        public int deadline { get; set; }
    }
}
