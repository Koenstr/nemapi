﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NemApi
{
    public class VerifiableAccount : UnverifiableAccount
    {
       public PrivateKey PrivateKey { get; internal set; }
        
       internal VerifiableAccount(Connection connection, PrivateKey privateKey) 
            : base(connection, new PublicKey(CryptoBytes.ToHexStringLower(PublicKeyConversion.ToPublicKey(privateKey))))
        {
            if (!StringUtils.OnlyHexInString(privateKey.RAW) || (privateKey.RAW.Length == 64 && privateKey.RAW.Length == 66))
                throw new ArgumentException("invalid private key");
            if (null == connection)
                throw new ArgumentNullException("Connection cannot be null");

            PrivateKey = privateKey;
        }

       public async void bootNodeAsync(string name)
        {
            var path = "/node/boot";
            var address = IPAddress.Parse(Connection.getUri().Host);

            var nodeData = new BootNodeRootObject();
                nodeData.metaData.application = "NIS";
                nodeData.identity.name = name;
                nodeData.identity.privateKey = StringUtils.ConvertToUnsecureString(this.PrivateKey.RAW);
                nodeData.endpoint.host = Connection.getUri().Host;
                nodeData.endpoint.port = Connection.getUri().Port;
                nodeData.endpoint.protocol = "http";

            await new AsyncConnector.PostAsync(Connection).post(path, nodeData); 
        }

       public async Task<NemAnnounceResponse.Response> sendTransactionAsync(TransferTransactionData transactionData)
        {
            var transfer = new TransferTransaction(this.Connection, transactionData.multisigAccount == null ? this.PublicKey : transactionData.multisigAccount, this.PrivateKey, transactionData);

            if (transactionData.multisigAccount == null)
            {
                return await new Prepare(Connection, PrivateKey).Transaction(
                    transfer.getTransferBytes());
            }
            else
            {
                var multisig = new MultiSigTransaction(this.Connection, this.PublicKey, this.PrivateKey, transactionData.deadline, transfer.FullLength);

                return await new Prepare(Connection, PrivateKey).Transaction(
                    ByteUtils.ConcatonatatBytes(
                        multisig.getBytes(), 
                        transfer.getTransferBytes()));
            }
        }

       public async Task<NemAnnounceResponse.Response> importanceTransferAsync(ImportanceTransferData data)
       {
            var transfer = new ImportanceTransfer(
                this.Connection, data.multisigAccount == null ? this.PublicKey : data.multisigAccount, data);
                
            if(data.multisigAccount == null)
            {
                return await new Prepare(this.Connection, this.PrivateKey).Transaction(transfer.getBytes());
            }
            else
            {
                 var multisig = new MultiSigTransaction(this.Connection, this.PublicKey, this.PrivateKey, data.deadline, transfer.FullLength);
            
                 return await new Prepare(Connection, PrivateKey).Transaction(
                     ByteUtils.ConcatonatatBytes(
                         multisig.getBytes(),
                         transfer.getBytes()));
             }
        }

       public async Task<NemAnnounceResponse.Response> aggregateMultisigModificationAsync(AggregateModificationData data)
       {
           var aggregateModification = new AggregateModificatioList(this.Connection, data.multisigAccount, data);     
 
           var multisig = new MultiSigTransaction(this.Connection, this.PublicKey, this.PrivateKey, data.deadline, aggregateModification.Length);
           
           return await new Prepare(Connection, PrivateKey).Transaction(
               ByteUtils.ConcatonatatBytes(
                   multisig.getBytes(),
                   aggregateModification.getBytes()));
              
        }

       public async Task<NemAnnounceResponse.Response> provisionNamespaceAsync(ProvisionNameSpaceData data)
       {
            var NameSpace = new ProvisionNamespace(Connection, data.multisigAccount == null ? this.PublicKey : data.multisigAccount, data);

            if(data.multisigAccount == null)
            {
                return await new Prepare(this.Connection, this.PrivateKey).Transaction(NameSpace.getBytes());
            }
            else{
                var multisig = new MultiSigTransaction(this.Connection, this.PublicKey, this.PrivateKey, data.deadline, NameSpace.Length);

                return await new Prepare(Connection, PrivateKey).Transaction(
                    ByteUtils.ConcatonatatBytes(
                        multisig.getBytes(),
                        NameSpace.getBytes()));
            }
           
       }
   
       public async Task<NemAnnounceResponse.Response> createMosaicAsync(MosaicCreationData data)
       {
            var model = new MosaicDefinitionModel(data);

            var mosaic = new CreateMosaic(this.Connection, data.multisigAccount == null ? this.PublicKey : data.multisigAccount,model);

            if (data.multisigAccount == null)
            {
                return await new Prepare(this.Connection, this.PrivateKey).Transaction(mosaic.getBytes());
            }
            else
            {
                var multisig = new MultiSigTransaction(this.Connection, this.PublicKey, this.PrivateKey, data.deadline, mosaic.Length);

                return await new Prepare(Connection, PrivateKey).Transaction(
                     ByteUtils.ConcatonatatBytes(
                         multisig.getBytes(),
                         mosaic.getBytes()));
            }
        }

       public async Task<NemAnnounceResponse.Response> mosaicSupplychangeAsync(MosaicSupplyChangeData data)
        {
            var change = new SupplyChange(this.Connection, data.multisigAccount == null ? this.PublicKey : data.multisigAccount, data);

            if (data.multisigAccount == null)
            {
                return await new Prepare(this.Connection, this.PrivateKey).Transaction(change.getBytes());
            }
            else
            {
                var multisig = new MultiSigTransaction(this.Connection, this.PublicKey, this.PrivateKey, data.deadline, change.Length);

                return await new Prepare(Connection, PrivateKey).Transaction(
                     ByteUtils.ConcatonatatBytes(
                         multisig.getBytes(),
                         change.getBytes()));
            }
        }

        public async Task<NemAnnounceResponse.Response> signMultisigTransaction(MultisigSignatureTransactionData data)
        {
            var signature = new MultisigSignature(this.Connection, this.PublicKey, data);

            return await new Prepare(this.Connection, this.PrivateKey).Transaction(signature.getBytes());   
        }
    }
}

       
