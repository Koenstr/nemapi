﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    internal class Prepare
    {
        private Connection Connection { get; set; }
        private PrivateKey PrivateKey { get; set; }

        internal Prepare(Connection connection, PrivateKey privateKey)
        {
            this.Connection = connection;
            this.PrivateKey = privateKey;
        }

        internal async Task<NemAnnounceResponse.Response> Transaction(byte[] bytes)
        {
            if (null == bytes)
                throw new ArgumentNullException("Byte array cannot be null");
            
            string r = CryptoBytes.ToHexStringUpper(new Signature(bytes, this.PrivateKey).LongSignature);


            AsyncConnector.PostAsync transaction = new AsyncConnector.PostAsync(this.Connection)
            {
                RPA = new ByteArrayWtihSignature()
                {
                    data = CryptoBytes.ToHexStringUpper(bytes),

                    signature = r.ToUpper()
                }
               
            };

            return await transaction.send();
        }
    }
}
