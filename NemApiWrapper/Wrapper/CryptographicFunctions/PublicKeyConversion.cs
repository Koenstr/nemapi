﻿using Chaos.NaCl;
using Org.BouncyCastle.Crypto.Digests;
using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace NemApi
{
    /*
    * Converts a private key to public key
    * and converts a public key to encoded address.
    *
    * Note: Nem uses sha3 for public key encryption and address encoding. 
    *       Sha3 was not available in the BouncyCastle Cryptography library
    *       so the Chaos.NaCl implementation of Sha3 was imported into the 
    *       BouncyCastle library for use in this wrapper.
    *
    * Note 2: Bouncy castles Ed25519 does not by default support 66 char
    *         private keys so this also had to be updated within the bouncycastle
    *         library, specifically in the Ed25519 class. 
    */
    public class PublicKeyConversion
    {
        /*
        * Converts a provided private key to a public key
        *
        * @Param: privatrKey, The key to convert to a public key
        *
        * @Returns: PublicKey
        */
        public static byte[] ToPublicKey(PrivateKey privateKey)
        {
            if (!StringUtils.OnlyHexInString(privateKey.RAW) || (privateKey.RAW.Length == 64 && privateKey.RAW.Length == 66))
                throw new ArgumentException("invalid private key");

                byte[] PrivateKeyArray = CryptoBytes.FromHexString(StringUtils.ConvertToUnsecureString(privateKey.RAW));

                Array.Reverse(PrivateKeyArray);

                return Ed25519.PublicKeyFromSeed(PrivateKeyArray);
        }


    }
}

