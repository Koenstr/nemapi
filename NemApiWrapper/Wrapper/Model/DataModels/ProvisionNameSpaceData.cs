﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class ProvisionNameSpaceData
    {
        public PublicKey multisigAccount { get; set; }
        public string newPart { get; set; }
        public string parent { get; set; }
        public int deadline { get; set; }
    }
}
