﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class Nis
    {
        private Connection Connection { get; set; }

        public Nis(Connection connection)
        {
            Connection = connection;
        }

        public async Task<TimeSync> TimeSync()
        {
            return await new AsyncConnector.GetAsync<TimeSync>(Connection).get("/debug/time-synchronization");
        }

        public async Task<HeartBeat> HeartBeat()
        {
            return await new AsyncConnector.GetAsync<HeartBeat>(Connection).get("/heartbeat");
        }

        public async Task<Status> Status()
        {
            return await new AsyncConnector.GetAsync<Status>(Connection).get("/status");
        }

        public async Task<NetworkTime> NetworkTime()
        {
            return await new AsyncConnector.GetAsync<NetworkTime>(Connection).get("/time-sync/network-time");
        }
    }
}
