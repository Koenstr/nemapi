﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Org.BouncyCastle.Math;

namespace NemApi
{
    internal class Transaction
    {
        private Serializer Serializer = new Serializer();
        private byte[] CommonTransactionBytes { get; set; }
        private PublicKey PublicKey { get; set; }
        private int NetworkVersion { get; set; }
        private int TimeStamp { get; set; }
        private int Deadline { get; set; }            
       
        internal Transaction(Connection connection, PublicKey publicKey,  int deadline)
        {
            this.NetworkVersion = connection.getNetworkVersion();
            this.TimeStamp = TimeDateUtils.EpochTimeInMilliSeconds();
            this.Deadline = deadline == 0 ? this.TimeStamp + 1000 : this.TimeStamp + deadline;
            this.PublicKey =  publicKey;

            Serialize();
        }

        internal void UpdateTransactionType(int type)
        {
            Serializer.UpdateNthFourBytes(0, type);
        }

        internal void UpdateTransactionVersion(byte version)
        {
            Serializer.UpdateNthByte(4, version);
        }

        internal void UpdateFee(long fee)
        {
            Serializer.UpdateNthEightBytes(48, fee);
        }

        internal byte[] getCommonTransactionBytes()
        {        
            return ByteUtils.TruncateByteArray(Serializer.GetBytes(), StructureLength.TransactionCommon);
        }

        private void Serialize()
        {
            // transaction type. Set as null/empty bytes as it will be 
            // updated when serializing the different transaction types.
            this.Serializer.WriteBytes(DefaultBytes.ZeroByteValue);

            // version
            this.Serializer.WriteCustomBytes(DefaultValues.ZeroValuePlaceHolder, this.NetworkVersion);

            // timestamp
            this.Serializer.WriteInt(this.TimeStamp);
            
            //pubKey len
            this.Serializer.WriteInt(ByteLength.PublicKeyLength);

            // public key
            Serializer.WriteBytes(CryptoBytes.FromHexString(this.PublicKey.RAW));

            // fee
            this.Serializer.WriteLong(DefaultValues.ZeroValuePlaceHolder);

            // deadline
            this.Serializer.WriteInt(this.Deadline);
        }
    }
}
