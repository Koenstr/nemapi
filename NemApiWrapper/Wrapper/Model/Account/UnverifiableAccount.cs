﻿using Chaos.NaCl;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    /*
    * Creates an instance of an unverfiable account
    */
    public class UnverifiableAccount
    {
        internal Connection Connection { get; set; }
        public PublicKey PublicKey { get; set; }
        public EncodedAddress Encoded { get; set; }

        internal UnverifiableAccount(Connection connection, PublicKey publicKey)
        {
            if (!StringUtils.OnlyHexInString(publicKey.RAW) || publicKey.RAW.Length != 64) 
                throw new ArgumentException("invalid public key");

            this.Connection = connection;
            this.PublicKey = publicKey;
           
            this.Encoded = new EncodedAddress(AddressEncoding.ToEncoded(Connection.getNetworkVersion(), PublicKey));   
        }

        internal UnverifiableAccount(Connection connection, EncodedAddress address)
        {
            if (address.ENCODED.Length != 40)
                throw new ArgumentException("invalid Address");
            this.Connection = connection;
            this.Encoded = address;
     
            try
            {
                var a = AccountInfoFromAddress(address.ENCODED).Result;

                if (a.account.publicKey != null)
                    this.PublicKey = new PublicKey(a.account.publicKey);
            }
            catch
            {
               // throw new ArgumentException("Cannot get pub key");
            }
        }

        public async Task<Transactions.List> getAllTransactionsAsync(string hash = null, int id = 0)
        {
            var path = "/account/transfers/all";

            var query = (id == 0 && hash == null) ? string.Concat("address=", this.Encoded.ENCODED)
                        : id == 0 && hash != null ? string.Concat("address=", this.Encoded.ENCODED, "&hash=", hash)
                                                  : string.Concat("address=", this.Encoded.ENCODED, "&hash=", hash, "&id=", id);

            var transactions = await new AsyncConnector.GetAsync<Transactions.List>(Connection).get(path, query);

            foreach (Transactions.TransactionData t in transactions.data)
            {
                if (null != t.Transaction.otherTrans.message.payload)
                {
                    t.Transaction.otherTrans.message.payload = Encoding.GetEncoding("UTF-8").GetString(CryptoBytes.FromHexString(t.Transaction.otherTrans.message.payload));
                }
            }

            return transactions;
        }
        
        public async Task<Transactions.List> getIncomingTransactionsAsync(string hash = null, int id = 0)
        {
            var path = "/account/transfers/incoming";

            var query = (id == 0 && hash == null) ? string.Concat("address=", this.Encoded.ENCODED)
                       : id == 0 && hash != null  ? string.Concat("address=", this.Encoded.ENCODED, "&hash=", hash)
                                                  : string.Concat("address=", this.Encoded.ENCODED, "&hash=", hash, "&id=", id);


            var transactions = await new AsyncConnector.GetAsync<Transactions.List>(Connection).get(path, query);

            foreach (Transactions.TransactionData t in transactions.data)
            {
                if (null != t.Transaction.otherTrans.message.payload)
                {
                    t.Transaction.otherTrans.message.payload = Encoding.GetEncoding("UTF-8").GetString(CryptoBytes.FromHexString(t.Transaction.otherTrans.message.payload));
                }
            }

            return transactions;
        }

        public async Task<Transactions.List> getOutgoingTransactionsAsync(string hash = null, int id = 0)
        {
            var path = "/account/transfers/outgoing";

            var query = (id == 0 && hash == null) ? string.Concat("address=", this.Encoded.ENCODED)
                       : id == 0 && hash != null  ? string.Concat("address=", this.Encoded.ENCODED, "&hash=", hash)
                                                  : string.Concat("address=", this.Encoded.ENCODED, "&hash=", hash, "&id=", id);
            var transactions = await new AsyncConnector.GetAsync<Transactions.List>(Connection).get(path, query);

            foreach (Transactions.TransactionData t in transactions.data)
            {
                if (null != t.Transaction.otherTrans.message.payload)
                {
                    t.Transaction.otherTrans.message.payload = Encoding.GetEncoding("UTF-8").GetString(CryptoBytes.FromHexString(t.Transaction.otherTrans.message.payload));
                }
            }
           
            return transactions;
        }

        public async Task<Transactions.UnconfirmedList> getUnconfirmedTransactionsAsync()
        {
            var path = "/account/unconfirmedTransactions";

            var query = string.Concat("address=", this.Encoded.ENCODED);

            var transactions = await new AsyncConnector.GetAsync<Transactions.UnconfirmedList>(this.Connection).get(path, query);

            foreach (Transactions.UnconfirmedTransactionData t in transactions.data)
            {
                if (null != t.Transaction.otherTrans.message.payload)
                {
                    t.Transaction.otherTrans.message.payload = Encoding.GetEncoding("UTF-8").GetString(CryptoBytes.FromHexString(t.Transaction.otherTrans.message.payload));
                }
            }
            
            return transactions;
        }

        public async Task<HarvestingData.ListData> getHarvestingInfoAsync(string hash = null)
        {
            if (null == hash)
            {
                var block = new Block(this.Connection);

                var lastBlock = block.Last().Result;

                hash = lastBlock.prevBlockHash.data;
            }
            
            var path = "/account/harvests";

            var query = string.Concat("address=", Encoded.ENCODED, "&hash=", hash);

            var data = await new AsyncConnector.GetAsync<HarvestingData.ListData>(this.Connection).get(path, query);

            return data;
        }

        public async Task<Importances.ListImportances> getImportancesAsync()
        {
            var data = await new AsyncConnector.GetAsync<Importances.ListImportances>(this.Connection).get("/account/importances");

            return data;
        }

        public async Task<GeneratedKeyPair> getGenerateNewAccountAsync()
        {
            var data = await new AsyncConnector.GetAsync<GeneratedKeyPair>(this.Connection).get(string.Concat("/account/generate"));

            return data;
        }

        public async Task<HistoricData> HistoricData(string address, long start, long end, int increment)
        {
            var path = "/account/historical/get";

            var query = string.Concat("address=", address, "&startHeight=", start, "&endHeight=", end, "&increment=", increment);

            var data = await new AsyncConnector.GetAsync<HistoricData>(this.Connection).get(path, query);

            return data;
        }

        internal async Task<ExistingAccount.Data> AccountInfoFromAddress(string address)
        {
            var path = "/account/get";

            var query = string.Concat("address=", address);

            var data = await new AsyncConnector.GetAsync<ExistingAccount.Data>(this.Connection).get(path, query);

            return data;
        }

        public async Task<ExistingAccount.Data> getAccountInfoAsync()
        {
            var path = this.PublicKey.RAW == null ? "/account/get" : "/account/get/from-public-key";

            var query = this.PublicKey.RAW == null ? string.Concat("address=", Encoded.ENCODED) : string.Concat("publicKey=", PublicKey.RAW);

            var data = await new AsyncConnector.GetAsync<ExistingAccount.Data>(this.Connection).get(path, query);

            return data;
        }


        public async Task<AccountForwarded.Data> getDelegatedAccountRootAsync()
        {
            var path = PublicKey.RAW == null ? "/account/get/forwarded" : "/account/get/forwarded/from-public-key";

            var query = this.PublicKey.RAW == null ? string.Concat("address =", this.Encoded.ENCODED)
                                                   : string.Concat("publicKey=", this.PublicKey.RAW);

            var data = await new AsyncConnector.GetAsync<AccountForwarded.Data>(this.Connection).get(path, query);

            return data;
        }

        public async Task<Account.Status> getAccountStatusAsync()
        {
            var path = "/account/status";

            var query = string.Concat("address=", this.Encoded.ENCODED);

            var data = await new AsyncConnector.GetAsync<Account.Status>(this.Connection).get(path, query);

            return data;
        }

        public async Task<Definition.List> getMosaicsByNameSpaceAsync(string namespaceId, string id  = null, int pageSize = 0)
        {
           var path = "/namespace/mosaic/definition/page";

           var query = id == null && pageSize == 0 ? string.Concat("namespace=", namespaceId)
                     : id != null && pageSize == 0 ? string.Concat("namespace=", namespaceId, "&id=", id)
                                                   : string.Concat("namespace=", namespaceId, "&id=", id, "&pageSize=", pageSize);

            var data = await new AsyncConnector.GetAsync<Definition.List>(this.Connection).get(path, query);

            return data;
        }

        public async Task<MosaicDefinitions.List> getMosaicsAsync(string id = null, int pageSize = 0)
        {
            var path = "/account/mosaic/definition/page";

            var query = id == null && pageSize == 0 ? string.Concat("address=", Encoded.ENCODED)
                      : id != null && pageSize == 0 ? string.Concat("address=", Encoded.ENCODED, "&id=", id)
                                                    : string.Concat("address=", Encoded.ENCODED, "&id=", id, "&pageSize=", pageSize);

            var data = await new AsyncConnector.GetAsync<MosaicDefinitions.List>(this.Connection).get(path, query);

            return data;
        }

        public async Task<MosaicsOwned.RootObject> getMosaicsOwnedAsync()
        {
            var path = "/account/mosaic/owned";

            var query = string.Concat("address=", this.Encoded.ENCODED);

            var data = await new AsyncConnector.GetAsync<MosaicsOwned.RootObject>(this.Connection).get(path, query);

            return data;
        }

        public async Task<NameSpaceList> getNamespacesAsync(
            string parent = null, string id = null, int pageSize = 0)
        {
            var path = "/account/namespace/page";

            var query = parent == null && id == null && pageSize == 0 ? string.Concat("address=", this.Encoded.ENCODED)
                      : parent != null && id == null && pageSize == 0 ? string.Concat("address=", this.Encoded.ENCODED, "&parent=", parent)
                      : parent != null && id != null && pageSize == 0 ? string.Concat("address=", this.Encoded.ENCODED, "&parent=", parent, "&id=", id)
                                                                      : string.Concat("address=", this.Encoded.ENCODED, "&parent=", parent, "&id=", id, "&pageSize=", pageSize);

            var data = await new AsyncConnector.GetAsync<NameSpaceList>(this.Connection).get(path, query);

            return data;
        }
    }
}