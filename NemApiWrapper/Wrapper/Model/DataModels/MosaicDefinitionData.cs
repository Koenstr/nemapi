﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class MosaicCreationData
    {
        public PublicKey multisigAccount { get; set; }
        public int deadline { get; set; }
        public string nameSpaceId { get; set; }
        public string mosaicName { get; set; }
        public string description { get; set; }
        public int divisibility { get; set; }
        public long initialSupply { get; set; }
        public bool supplyMutable { get; set; }
        public bool transferable { get; set; }
        public MosaicLevy mosaicLevy { get; set; }
    }
}
