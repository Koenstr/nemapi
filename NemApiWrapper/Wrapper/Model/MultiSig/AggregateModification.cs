﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    /*
    * Creates an aggregate modification
    */
    public class AggregateModification
    {
        internal int ModificationType { get; set; }
        internal PublicKey PublicKey { get; set; }

        /*
        * Constructs a modification
        *
        * @Param: PublicKey, The public key of the signatory to modify
        * @Param: ModType, The type of modification (1 = add, 2 = remove)
        */
        public AggregateModification(PublicKey publicKey, int modType)
        {
            if (publicKey == null)
                throw new ArgumentNullException("Public key cannot be null");
            if (modType != 1 && modType != 2)
                throw new ArgumentException("Modification type invalid");

            this.ModificationType = modType;
            this.PublicKey = publicKey;
        }
    }
}
