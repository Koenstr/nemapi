﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class MultisigSignatureTransactionData
    {
        public string transactionHash { get; set; }
        public int deadline { get; set; }
        public EncodedAddress MultisigAddress { get; set; }
    }
}
