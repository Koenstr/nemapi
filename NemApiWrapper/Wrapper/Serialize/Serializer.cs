﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    /*
    * Serializer contains a number of methods which convert verious data to bytes,
    * then writing then to a MemoryStream.
    */
    internal class Serializer
    {
        internal MemoryStream Stream = new MemoryStream();
        BinaryWriter Writer { get; set; }

        internal Serializer()
        {
            this.Writer = new BinaryWriter(this.Stream);
        }
        
        internal void WriteString(string src)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(src);

            this.Writer.Write(bytes);
        }

        internal void WriteCustomBytes(int src, int src2)
        {
            var bytes = new byte[4];
                bytes[0] = (byte)src;
                bytes[3] = (byte)src2;

            this.Writer.Write(bytes);
        }

        internal void WriteBytes(byte[] src)
        {
            this.Writer.Write(src);
        }

        internal void WriteInt(int src)
        {
            var bytes = new byte[] {
                (byte)src,
                (byte)(src >> 8),
                (byte)(src >> 16),
                (byte)(src >> 24)};
            this.Writer.Write(bytes);
        }

        internal void WriteLong(long src)
        {
            this.WriteInt((int)src);
            this.WriteInt((int)(src >> 32));
        }

        internal byte[] GetBytes()
        {
            return this.Stream.GetBuffer();
        }
        
        internal void UpdateNthFourBytes(long index, int newValue)
        {
            var currPos = this.Stream.Position;
            try
            {
                var offset = index;
                this.Stream.Position = offset;
                this.Writer.Write(newValue);
            }
            finally { Stream.Position = currPos; }
        }
        internal void UpdateNthEightBytes(long index, long newValue)
        {
            var currPos = this.Stream.Position;
            try
            {
                var offset = index;
                this.Stream.Position = offset;
                this.Writer.Write(newValue);
            }
            finally { Stream.Position = currPos; }
        }

        internal void UpdateNthByte(long index, byte newValue)
        {
            var currPos = Stream.Position;
            try
            {
                var offset = index;
                this.Stream.Position = offset;
                this.Writer.Write(newValue);
            }
            finally { this.Stream.Position = currPos; }
        }
    }
}
