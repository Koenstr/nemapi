﻿
using Chaos.NaCl;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Modes;
using Org.BouncyCastle.Crypto.Paddings;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class Ed25519BlockCipher
    { 
       private VerifiableAccount VerifiableAccount { get; set; }
       private UnverifiableAccount UnverifiableAccount  { get; set; }
       private SecureRandom Random { get; set; }

       public Ed25519BlockCipher(VerifiableAccount verifiableAccount, UnverifiableAccount unverifiableAccount)
       {
           this.UnverifiableAccount = unverifiableAccount;
           this.VerifiableAccount = verifiableAccount;
           this.Random = new SecureRandom();
       }

        
       public byte[] Encrypt(byte[] input)
        {
            // Setup salt.
            byte[] salt = new byte[ByteLength.PublicKeyLength];
            this.Random.NextBytes(salt);

            // Derive shared key.
            byte[] sharedKey = GetSharedKey(this.VerifiableAccount.PrivateKey, this.UnverifiableAccount.PublicKey, salt);

            // Setup IV.
            byte[] ivData = new byte[16];
            
            this.Random.NextBytes(ivData);

            // Setup block cipher.
            BufferedBlockCipher cipher = this.SetupBlockCipher(sharedKey, ivData, true);

            // Encode.
            byte[] buf = Transform(cipher, input);
            if (null == buf)
            {
                return null;
            }

            byte[] result = new byte[salt.Length + ivData.Length + buf.Length];
            Array.Copy(salt, 0, result, 0, salt.Length);
            Array.Copy(ivData, 0, result, salt.Length, ivData.Length);
            Array.Copy(buf, 0, result, salt.Length + ivData.Length, buf.Length);
            return result;
       }
        
        public byte[] Decrypt(byte[] input)
        {
            if (input.Length < 64)
            {
                return null;
            }
            byte[] salt = new byte[32];
            byte[] ivData = new byte[16];
            byte[] encData = new byte [input.Length - 48];

            Array.ConstrainedCopy(input, 0, salt, 0, 32);
            Array.ConstrainedCopy(input, 32, ivData, 0, 16);
            Array.ConstrainedCopy(input, 48, encData, 0, input.Length - 48);

            // Derive shared key.
            byte[] sharedKey = this.GetSharedKey(this.VerifiableAccount.PrivateKey, this.UnverifiableAccount.PublicKey, salt);

            // Setup block cipher.
            BufferedBlockCipher cipher = this.SetupBlockCipher(sharedKey, ivData, false);

            // Decode.
            return this.Transform(cipher, encData);
        }
        
        private byte[] Transform(BufferedBlockCipher cipher, byte[] data)
        {
            byte[] buf = new byte[cipher.GetOutputSize(data.Length)];
            int length = cipher.ProcessBytes(data, 0, data.Length, buf, 0);
            try
            {
                length += cipher.DoFinal(buf, length);
            }
            catch (InvalidCipherTextException) {
                return null;
            }
            byte[] final = new byte[length];

            Array.Copy(buf, final, length);
            return final;
            }

        private BufferedBlockCipher SetupBlockCipher(byte[] sharedKey, byte[] ivData, bool forEncryption)
        {
            // Setup cipher parameters with key and IV.
            var keyParam = new KeyParameter(sharedKey);
            var param = new ParametersWithIV(keyParam, ivData);

            // Setup AES cipher in CBC mode with PKCS7 padding.
            var padding = new Pkcs7Padding();

            BufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new CbcBlockCipher(new AesEngine()), padding);
            cipher.Reset();
            cipher.Init(forEncryption, param);
            return cipher;
        }
        
       private byte[] GetSharedKey(PrivateKey privateKey, PublicKey publicKey, byte[] salt)
       {
           var shared = new byte[32];
      
           var hash = Ed25519.key_derive(
               shared, 
               salt, 
               CryptoBytes.FromHexString(publicKey.RAW), 
               CryptoBytes.FromHexString(StringUtils.ConvertToUnsecureString(privateKey.RAW)));
      
      
           return shared;
       }   
        
    } 
}
