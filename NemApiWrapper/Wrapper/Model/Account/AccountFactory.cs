﻿using Chaos.NaCl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace NemApi
{
    public class AccountFactory
    {
        private Connection Connection { get; set; }

        public AccountFactory(Connection connection)
        {
            if (null == connection) {
                throw new ArgumentNullException("Connection cannot be null");
            }
            this.Connection = connection;
        }

        public AccountFactory()
        {
            this.Connection = new Connection();
        }

        public VerifiableAccount fromPrivateKey(string key)
        {
            if (!StringUtils.OnlyHexInString(key) || (key.Length != 64 && key.Length != 66))
            {
                throw new ArgumentException("invalid private key");
            }
            return new VerifiableAccount(this.Connection, new PrivateKey(key));
        }

        public UnverifiableAccount fromPublicKey(string publicKey)
        {
            if (!StringUtils.OnlyHexInString(publicKey) || (publicKey.Length != 64 && publicKey.Length != 66))
            {
                throw new ArgumentException("invalid public key");
            }
            return new UnverifiableAccount(this.Connection, new PublicKey(publicKey));
        }

        public VerifiableAccount fromPrivateKey(PrivateKey key)
        {
            if (!StringUtils.OnlyHexInString(key.RAW) || (key.RAW.Length != 64 && key.RAW.Length != 66)) {
                throw new ArgumentException("invalid private key");            
            }
            return new VerifiableAccount(this.Connection, key);
        }

        public UnverifiableAccount fromPublicKey(PublicKey publicKey)
        {
            if (!StringUtils.OnlyHexInString(publicKey.RAW) || (publicKey.RAW.Length != 64 && publicKey.RAW.Length != 66)){
                throw new ArgumentException("invalid public key");   
            }
            return new UnverifiableAccount(this.Connection, publicKey);
        }

        public UnverifiableAccount fromEncodedAddress(string EncodedAddress)
        {
            return new UnverifiableAccount(this.Connection, new EncodedAddress(EncodedAddress));
        }

        public UnverifiableAccount fromEncodedAddress(EncodedAddress EncodedAddress)
        {
            return new UnverifiableAccount(this.Connection, EncodedAddress);
        }
    }
}
